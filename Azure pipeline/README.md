# Azure Devops

![Alt Text](https://planview-media.s3.us-west-2.amazonaws.com/wp-content/uploads/2022/10/azure_devops_-_new_logo.png)


Microsoft's Azure Devops presents itself as a Software as a Service (SaaS) platform, providing a comprehensive suite of DevOps tools to facilitate and shorten software development cycle from source code management and continuous integration to deployment and monitoring. Its capability to integrate with leading industry tools positions it as an optimal solution for orchestrating DevOps services, making it an apt fit for our use case.  

![Alt Text](https://softobiz.com/wp-content/uploads/2019/11/devops-2.jpg)

Although there are multiple DevOps plateform that can achieve the same results, We take a particular interest in this solution's flexibilty which is its most prominent feature, as it can effectively accommodate different languages, platforms and cloud environments. It can for instance easily integrate with other solutions, Github for instance and Gitlab , and it is cloud independent despite it being a property of Azure's Microsoft. You can learn more about the plateform in the official [Azure DevOps documentation](https://learn.microsoft.com/en-us/azure/devops/?view=azure-devops).
## Azure pipeline

Azure Pipelines automatically builds and tests code projects. It supports continuous integration (CI) and continuous delivery (CD) to continuously test, build, and deploy your code as in every other CI/CD tool by defining a pipeline that is a set of multiple automated tasks, written in YAML. 

The focus of this hands on is on defining and running a pipeline to deploy and test the [SLICES Blueprint](https://slices-sc.eu/). 


## Architecture of the Blueprint testing environment
![](./architecture_of_azure.png)
In this Hands-on we will proceed with our manipulation and deployment by doing our setup inside the Z1 sophia node at INRIA that we have access to. 

To run DevOps pipeline, we need a  "DevOps Agent" which does various tasks, such as:checking out and compling source code, run tasks and scripts, and create or publish artifacts. By default Azure can provide Microsoft-hosted agents hosted in their own premises, but can only give free limited access for one agent with a private project, check [capabilities and limitations](https://learn.microsoft.com/en-us/azure/devops/pipelines/agents/hosted?view=azure-devops&tabs=yaml#capabilities-and-limitations) of the free tier.

 We use a self hosted azure agent in our premises that will run the pipeline from a docker container inside the azure VM. Running agents in containers is a great solution for achieving flexibility and to have a more granular control over the agents dependencies and configurations through a Dockerfile. We personalize the container according to the requirements. Certain tasks and scripts may rely on particular tools existing within the container's PATH, it is therefore our responsibility to make sure they are available. The choice of having the container in a VM is mainly, but not only, to have root permissions.

We need to give our agent access to Z1 with SSH, only then we can create our VMs and clusters. The purpose is to test the deployment of the blueprint for a specific OS-verion, say ubuntu 22.04. For this purpose in our case the agent through the pipeline creates multiple multipass VMs that will play the role of k8s nodes, you can use the cloud-init.yaml template to specify the public key so that the agent can SSH to the VMs when running the playbooks. For that we'll also need to obtain the VMs IP@ to put them in the ansible host files, and then we can run the agent will be able to correctly run the ansible playbooks. After doing an interconnection test and making sure everything is working as intended, the VMs are destroyed when the pipeline comes to an end and everything is cleaned up by the agent.

## Install self hosted agent 

Before installing the self hosted agent, there are some steps to carry on with before that, such as setting the right permissions. You can check the full steps [here](https://learn.microsoft.com/en-us/azure/devops/pipelines/agents/linux-agent?view=azure-devops#check-prerequisites)

### Authenticate with a personal access token

The user configuring the agent needs pool admin permissions, but the user running the agent does not and we need to create a personal access token in order to do that. It will not use credentials in everyday operation, but they're required to complete registration.

Go to user settings and click on personal access tokens, add a new token and make sure to store it somewhere because we will use it later.

### Download and configure the agent
![Alt Text](./agentpools.png)
After the previous step, go to project settings, select agent pools and then add a new pool, follow the steps as shown in the [documentation](https://learn.microsoft.com/en-us/azure/devops/pipelines/agents/linux-agent?view=azure-devops#download-and-configure-the-agent)
But mainly, after creating the pool click on it and then go to agents to add a new agent.

![Alt Text](./agentpools.png)

![agent](./agent.png)
You will have a pop up in which there is a link to download the zip file containing the agent configuration from the provided link after adding the agent and follow the instructions after choosing your OS, unpack the agent into the directory of your choice in your agent host and run ./config.sh.

### Run the self-hosted agent in Docker

Set up a self-hosted agent in Azure Pipelines to run inside an Ubuntu container with Docker, you can refer to [azure's article](https://learn.microsoft.com/en-us/azure/devops/pipelines/agents/docker?view=azure-devops#linux) about this.
IN our case we have the following content in our Dockerfile: 


```
FROM ubuntu:20.04
RUN DEBIAN_FRONTEND=noninteractive apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get upgrade -y

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y -qq --no-install-recommends \
    apt-transport-https \
    apt-utils \
    ca-certificates \
    curl \
    git \
    iputils-ping \
    jq \
    lsb-release \
    software-properties-common \
    ssh

RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash
RUN DEBIAN_FRONTEND=noninteractive apt update && apt-get install -y python3-pip parallel
RUN DEBIAN_FRONTEND=noninteractive python3 -m pip install --user ansible-core==2.13.2 && echo "export PATH=$PATH:~/.local/bin" >> ~/.bashrc
RUN exec bash && . ~/.bashrc
RUN mkdir /reqs/
COPY collections/requirements.yml /reqs/ansible-galaxy.yml
COPY requirements.txt /reqs/python3.txt
RUN ~/.local/bin/ansible-galaxy install -r /reqs/ansible-galaxy.yml
RUN pip3 install -r /reqs/python3.txt
# Can be 'linux-x64', 'linux-arm64', 'linux-arm', 'rhel.6-x64'.
ENV TARGETARCH=linux-x64

WORKDIR /azp

COPY ./start.sh .
RUN chmod +x start.sh

ENTRYPOINT [ "./start.sh" ]
```
You can add more packages or change the configuration to suit your use case, make sure to add ansible and all the requirements that the agent has to use. 

Build the dockerfile image and then run a container using it. Make sure to add the token so that the agent will have access to your account and put your organisation's name, we run it in detahc mode so that the agent can stay running in the background.
```
docker run -d --name azureagent -e AZP_URL=https://dev.azure.com/<NAME_OF_YOUR_ORGANISATION> -e AZP_TOKEN=<YOUR_TOKEN> -e AZP_AGENT_NAME=z1 -v ${HOME}/.ssh/id_rsa_blueprint:/id_rsa_blueprint dockeragent:latest
```
We add the private key of azure since it is easier to create a key pair in azure vm and then do a cloud-init with multipass when creating the VMs, but you can configure the pipeline such you use a key pair inside the agent though it will take more configuration.

>If you want the agent to use docker to build and push images you can consider mounting /var/run/docker.sock:/var/run/docker.sock.

```
docker run -d --name azureagent -e AZP_URL=https://dev.azure.com/<NAME_OF_YOUR_ORGANISATION> -e AZP_TOKEN=<YOUR_TOKEN> -e AZP_AGENT_NAME=z1 -v ${HOME}/.ssh/id_rsa_blueprint:/id_rsa_blueprint -v /var/run/docker.sock:/var/run/docker.sock dockeragent:latest
```

>If you change the name of the container make sure to change it in the pipeline too when doing docker cp to send IP@ file to the agent.

Don't forget to install `envsubst`, for example with the command

```
docker exec azureagent apt install gettext-base
```


### Configure SSH keys
After doing the previous steps normally you'll have a functionnal agent that can run your pipelines. Now we need to make sure that our agent has the right permissions for it to be able to ssh to Z1 server. From the graphical interface, go to project settings, service connections and then create a new SSH connection. You can then specify the name of your new connection and pass the private key for connecting to the endpoint, it's generally secure by default without the need of adding any layer of security for a normal usage. 

![service connections](./service-connections.png)

Keep in mind also when editing your pipeline that there are some template task on your right, one of them is the SSH task, you can use it after adding your service connection to create a task than needs to access your remote host as shown below. There are also other automated tasks like building and pushing to docker registery for instance, you just need to create a service connction where you specify that it is a docker service so that the agent will recognize your docker registry from the pipeline task requiring access to it if you want to push or pull.
```yml
- task: SSH@0
  inputs:
    sshEndpoint: 'sopnode-z1'
    runOptions: 'commands'
    commands: echo 'test'
    readyTimeout: '20000'
```

## Build the pipeline
Now you are ready to define your pipeline. First make sure to specify your agent pool: by definition an agent pool is a group of self-hosted or Microsoft-hosted agents that you can use to run your pipelines and jobs in Azure Pipelines.
If it's an Azure hosted VM, you'll use something like this for an ubuntu agent: 
```yml
 pool:
   vmImage: ubuntu-latest
```
The benefits for using a Microsoft hosted agent is that you can easily change the VM os and have a flexibility over the infrastructure, although at a cost if the free tier limit is exeeded.

Using a self hosted agent is a good alternative if you want to have full control over the infrastructure, it is also free and at your own cost when it comes to managing the infrastructure, and there are no request limitations, to use it you'll just need to specify the name of the pool that you created to add your host as an agent, if there is only one agent you don't have to specify the agent name:
```yml
pool:
 name: Default
 agent: z1
```
## Customize the pipeline
You can have a look at how to [customize your pipeline](https://learn.microsoft.com/en-us/azure/devops/pipelines/customize-pipeline?view=azure-devops), keep in mind that there are multiple ways how to implement a pipeline and there are many ways in which we can achieve the same results, we'll have a look at some standard customization.

### Triggers and pipeline scheduling
There are multiple ways to trigger a pipeline run, one of them is to do it on commit, you can specify the name of the branch or specify none if you want to prevent trigger behaviour :
```yml
trigger:
- main
#- none
```

But in most cases we don't want to have a pipeline running after a change especially on prod, usually we want to have nightly runs that will test our deployment in a testing environment and we can achieve that by having triggers that run following a schedule. One way to do is to have a cron schedule through the YAML file, something like this: 

```yml
schedules:
- cron: '0 0 * * *'
  displayName: Daily midnight build
  branches:
    include:
    - main
```
Check how to [Configure schedules for pipelines](https://learn.microsoft.com/en-us/azure/devops/pipelines/process/scheduled-triggers?view=azure-devops&tabs=yaml) for further details.

We can also do it from the interface by going to triggers section and then we can schedule a daily pipeline run or a customized schedule depending on the use case.

![triggers](./triggers.png)

![schedules](./schedules.png)



### Strategy matrix and multi-plateform support

We can use the strategy matrix in azure pipeline to shorten the code and to generate jobs in pipeline based on matrix of variables, it is useful when we want to test a configuration or in our case a deployment in different versions and build across multiple platforms. We can use variables to conveniently put data into various parts of a pipeline, like in the choice of the agents OS if you are using a Microsoft hosted agent as detailed in the [Azure Devops Documentation](https://learn.microsoft.com/en-us/azure/devops/pipelines/customize-pipeline?view=azure-devops#build-across-multiple-platforms) . 

We can also use it inside the pipeline like we did in our pipeline. In this hands-on we try to have a multi-os support that needs to be populated later with the actual pipeline, for now the pipline is for testing deployment in Ubuntu 22.04. Instead of multiplying lines of code we can use matrix in which we define the versions of the VMs that will created for instance, it'l look something like this: 
```yml  
  jobs:
  - job: BuildAndTestDeploymentOf 
    strategy:
      matrix:
        Ubuntu 20.04:
          os: 'ubuntu-latest'
          architecture: 'x64'
        windows 11:
          os: 'windows-latest'
          architecture: 'x64'
        macOS:
          os: 'macOS-latest'
          architecture: 'x64'
    steps:
    - script: echo Building and testing on $(os) $(architecture)
      displayName: 'Build and Test Deployment'
```
Note that the strategy is defined at the job level, what will happen with this strategy is that 3 jobs will be executed instead of one. The executed script will iterate on the different OSs and architecture that were defined in the matrix. There will be as many jobs as there are Matrix elements. When executed we have this behaviour:
![matrix](./matrixjobs.png)

![matrixexample](./matrixjobexample.png)

You can use this as a template for populating the different OS test deployments and use variables to customize each version deployment.

## Migrate between Github actions and Azure pipeline

Github actions and azure DevOps are both products of Microsoft. One would imagine that there wouldn't be much difference between the too and that may be right. In fact, the Actions agent is a fork of the Pipelines agent and there are many similarities between GitHub Actions and Azure Pipelines that make worthwhile comparing the too of them espacially if you are already well familiar with Github Actions syntax. You can check the [Compare and contrast GitHub Actions and Azure Pipelines article](https://learn.microsoft.com/en-us/dotnet/architecture/devops-for-aspnet-developers/actions-vs-pipelines) in the official documentation of Microsoft.






