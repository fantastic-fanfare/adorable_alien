# Configuring Kubernetes service in a Go backend
We have been developing a backend in Go that is responsible for authenticating users and grant access to some services including manipulating devices of the sophianode infrastructure (NetBox resource catalog). This service can be found (here)[] for more details. 

Keep in mind that we (backend) are granted an __token_id__ from an OIDC/OAuth2 authentication mechanism explained [here](https://gitlab.inria.fr/fantastic-fanfare/adorable_alien/-/tree/kooki/sc-login#2-bff-in-a-nutshell), that holds encoded identity information about the user, in the backend from the SLICES OIDC server in the process of user authentication/authorization. This token can be used to authenticate and thus create users inside k8s clusters via k8s API server.  But first of all, the backend should be configured and connected to the API server of kubernetes where the clusters are running. In our example, we created a kubernetes cluster, using **Kubeadm** and **Minikube** check [here](https://gitlab.inria.fr/fantastic-fanfare/adorable_alien/-/tree/develop/namespace%20isolation%20and%20RBAC%20hands%20on#creating-kubeadm-cluster-with-ansible) for more details.

## Access the API: Configuartion with go-client 
Kubernetes supports [client libraries](https://kubernetes.io/docs/tasks/administer-cluster/access-cluster-api/) for multiple languages including for Go to access the API. It is the `go-client` library that will allow the backend to talk to a k8s cluster. Check out the official [git repo](https://github.com/kubernetes/client-go) of the library.

We imported the necessary packages to our k8s module:
```shell
go get k8s.io/api/core/v1
go get k8s.io/api/rbac/v1
go get k8s.io/apimachinery/pkg/apis/meta/v1
go get k8s.io/api/networking/v1
go get k8s.io/client-go/kubernetes
go get k8s.io/client-go/tools/clientcmd
```
If you cloned the repository to your project directory (where k8s is configured as well), you can just run the following command to build the image and the container where all dependencies are downloaded, but make sure to **modify** the `ENV` variables according to your cluster IP and port (6443 if kubeadm cluster) as well as the path to your cluster kubeconfig file in the container where your local kubeconfig file will be mounted as declared in docker-compose file:
```dockerfile
ENV KUBERNETES_MASTER=<CLUSTER_IP_@>:<CLUSTER_API_LISTENNING_PORT>
ENV KUBECONFIG=<PATH_TO_KUBECONFIG_IN_CONTAINER> #Like /etc/kubernetes/admin.conf
```
Don't forget to map the k8s API server port to the container in docker-compose.yml like for instance `6443`:
```yaml
    ports: 
      - "8008:8008"
      - "6443:6443"
```
> #### Troubleshooting Minikube cluster access:
> 
> Particularily with Minikube setup, you should also think of mounting the certificate and key file to your container along with the kubeconfig file. Also, we recommend that you change the absolite paths in the kubeconfig file that points to the location of those files to avoid path/location conflicts when the backend tries to access the cluster. Related errors might look like the following samples:
> ![image err1](./err1.png)
> Or if mounted but the networking configuration (IP@ and ports) is not well installed, you can have the following error:
> ![image err2](./err2.png)
>
> KUBEADM: Don't worry this issue won't happen with kubeadm setup as everything is loaded inside the kubeconfig file.

After correctly updating the Dockerfile and docker-compose files, you can run the containers (backend will listen on port 8008, you can change it in main.go & in docker files):
```shell
docker compose up --build
```
The paramout to this configuration is the **kubecongif** file that comes handy to help authenticate the go client to the API server. Example: The kubectl command-line tool uses kubeconfig files to find the information it needs to choose a cluster and communicate with the API server of a cluster. We will follow the same approach with our trusted backend source.

*PS: The kubeconfig file contains the necessary information, such as the cluster address, authentication tokens, and certificates, to establish a connection with the cluster. Therefore, as long as the machine running the code has the correct kubeconfig file and proper network access to the Kubernetes API server, it can interact with the cluster.*

Thus we connect our backend to the k8s API server via the kubeconfig file from outside the cluster assuming that the cluster and from where the code is ran have no connectivty issues.

## Theory behind Kubernetes configuration with OIDC and usage of ID tokens
#### Configuring Kubernetes with OIDC
In light of the SLICES documentation around [light federation](https://doc.slices-sc.eu/testbed_owner/oauth.html) when comes to setting up not only the backend but also the k8s cluster with OIDC, we prepare the configuration with SLICES OIDC server by registering our service as an OAuth Client.

1. Enable OIDC in Kubernetes API Server:
- Configure the Kubernetes API server to enable OIDC by specifying the OIDC parameters in the kube-apiserver configuration file or by passing it in the command (see Simple Test with Minikube and Kubeadm).
- Set the --oidc-issuer-url flag to the URL of the OIDC provider's discovery endpoint, e.g., https://account.ilabt.imec.be.

1. Configure OIDC Client Parameters:
- Set the --oidc-client-id flag to the client ID provided by the OIDC provider for your Kubernetes cluster.
- Set the --oidc-ca-file flag to the path of the certificate authority (CA) file used to verify the OIDC provider's TLS certificate.
  
#### Usage of ID Tokens in Kubernetes

- **User Authentication**:  
When a user wants to interact with the Kubernetes API server, they authenticate themselves through the OIDC provider's authentication flow.
Upon successful authentication, the OIDC provider issues an ID token to the user (to the backend in our case).

- **API Request with ID Token**:  
To perform operations on the Kubernetes cluster, the user includes their ID token in the request headers when making API calls.
The ID token is added as a bearer token, following the OAuth 2.0 Bearer Token specification when setting the k8s config client.

- **ID Token Validation**:  
The Kubernetes API server receives the request and validates the user's ID token.
The API server verifies the token's signature using the OIDC provider's public keys to ensure it was issued by a trusted authority.
It checks the token's expiration time and claims to ensure it is meant for the Kubernetes API server.

- **User Authorization**:  
After successful ID token validation, the Kubernetes API server authorizes the user based on the user's identity in the Kubernetes RBAC system.
The RBAC system checks the user's identity against predefined roles and role bindings to determine what actions the user is allowed to perform.

- **API Operation**:  
If the user's ID token is successfully verified, and the user is authorized, the Kubernetes API server performs the requested operation and returns the response.

## Testing the configuration
#### Simple test: Gets nodes in the cluster in all namespaces
After pinpointing to the kubeconfig file correctly, you can query and use k8s resources on that cluster as much as you need. Here's an example of listing all the nodes in our `minikube` cluster of 3 nodes (originally).  
The code is made such that you will not be obliged to change the path to your kubeconfig apart if you have it in another path other than `$(HOME)/.kube/config`.
```go
func main() {
	var kubeconfig *string
	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()
	// uses the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}

  	// creates the clientset
  	clientset, err := kubernetes.NewForConfig(config)
  	if err != nil {
		panic(err.Error())
  	}

  	// access the API to list pods
  	nodes, _ := clientset.CoreV1().Nodes().List(context.TODO(), v1.ListOptions{})
  	fmt.Printf("There are %d nodes in the cluster\n", len(nodes.Items))	

    // access the API to create a user

}
```
When ran, it should return an output like the following (depending on the # of nodes you configured):
```
There are 3 nodes in the cluster
```

## Practice: K8s authenticate with OIDC tokens
Whether you choose Minikube or Kubeadm, make sure you have configured the right __kube.config__ file in the backend where all resource creation and token verification will occur.

##### With Minikube
In this tutorial we have used Minikube for simplicity and testing puposes. To configure the K8s API server, Minikube actually handles it by only passing the OIDC configs when starting the cluster. Having the `cliend-id` and the `oidc-issuer-url`, you should be ready to run the following command that will start a k8s cluster of 2 nodes such that the API server is configured with our OIDC provider:
```shell
minikube start \
  --nodes 2 \
  --profile=slices-test \
  --extra-config=apiserver.authorization-mode=RBAC \
  --extra-config=apiserver.oidc-issuer-url=https://account.ilabt.imec.be \
  --extra-config=apiserver.oidc-username-claim=sub \
  --extra-config=apiserver.oidc-client-id=n5qsyXbYTeI89uVG3XLmYr2r
```
> oidc-username-claim can take any key value that consitutes the id-token payload. For instance, later in the kubeadm oidc setup, we chose the 'username' claim instead of 'sub'. YOu can use 'email' as well since all the three claims explicitly refer to human-readable unique credentials of users.
 
Because we are testing it locally, we defined a URI where the redirection will occur from OIDC to our k8s API: `http://localhost:59785`. So let's add it in our kubeconfig file located in `$HOME/.kube/config` as follows (apiserver config):
```
current-context: minikube
apiserver:
  redirect-uri: http://localhost:59785
kind: Config
preferences: {}
```

To create a user manually, we use kubectl config set-credentials along with a username and its id-token. The username that you should use with this command is the username that you use to authenticate with your OIDC provider. This username is typically the same username that you use to log in to your institute account or OIDC provider.

For example, if your OIDC provider is Google, then the username that you would use with the kubectl config set-credentials command would be your Google email address. In our case, since you can authenticate through institute (INRIA) portal account, you should use your institute-related username like first username letter+fulllastname (just an example). The token passed below is a returned OIDC `id_token` after the authentication of the user `kchiboub` (it is just a sample token, not valid).
```
kubectl config set-credentials kchiboub --token eyJhbGccdsfdvergferIsInR5cCI6IkpXVCIsImtpZCI6ImVjMSJ9.eyJpc3MiOiJodHRwczovL2FjY291bnQuaWxhYnQuaW1lYy5iZSIsImF1ZCI6WyJuNXFzeVhiWVRlSTg5dVZHM1hMbVlyMnIiXSwiaWF0IjoxNjkwNDQ4NDM4LCJleHAiOjE2OTA0NTIwMzgsImF1dGhfdGltZSI6MTY5MDQ0ODMzMiwiYXRfaGFzaCI6IjVjTWg4MzREYVQzU3Q1V3I4VDNNMkEiLCJzdWIiOiJrY2hpYm91YkBpbnJpYS5mciIsInVzZXJuYW1lIjoia2NoaWJvdWIiLCJ1cm4iOiJ1cm46cHVibGljaWQ6SUROK2lsYWJ0LmltZWMuYmUrdXNlcitrY2hpYm91YiIsInByb2plY3RfdXJucyI6WyJ1cm46cHVibGljaWQ6SUROK2lsYWJ0LmltZWMuYmUrcHJvamVjdCtqZmVkLWJsdWVwcmludCJdLCJlbWFpbCI6Imthb3V0YXIuY2hpYm91YkBpbnJpYS5mciIsInB1YmxpY19zc2hfa2V5Ijoic3NoLXJzYSBBQUFBQjNOemFDMXljMkVBQUFBREFRQUJBQUFCQVFERXBjTXZpMnZYK1RVNlNrU3NxQzBybzJwR3JDMStpWkgralYyYndrNTF2SEc5WlVyUHhNL2tYcGJ0eFJzaWlTOW80adlasdmJHSJxc2pOWXFuS01XVnlyV2ZqOWZIMXBDajRVUEhscS9OTk1aWVVHSytjaTBaTG4wL05NTjMwYTB1VzJQY3hXS0FzUEJrNDFKellBRGRhZG9zN1ZQRmtvSi9zRmJXaG5DNEp1ekphdjVBOUlDb1pWNFNkY3NvaGI2VFVmcG55NkFKTDRMOGNYZmJqS054RlR0S3ltVHh2R3hwTUZEakIyMXpVS3I5eXdxekFELzBXeksvbi9VOWREV3hibGh6RU0wNjhua2NkZzNPNGJ2NnZHdUc4c1F5dDJRYzhoS09kOGE4eEgrMTBXaHB1MEZzYkNzZjduWkJBU2p2bEY0eGlzUTFOYTMifQ.v8ghUJk-3cDSAJDuuWpe2GAJtvCCbkWmA5elmFUNgztXu-8n4bYYTJLbUsieRvOe4RfmDt_A

User "kchiboub" set.
```

##### With kubeadm:
If you are running a k8s cluster (not in minikube) and want to confiure the API server through kubectl, you should set up the k8s cluster first following this [tutorial](https://gitlab.inria.fr/fantastic-fanfare/adorable_alien/-/tree/develop/namespace%20isolation%20and%20RBAC%20hands%20on) straight to the second section and below.

- After we initialized the master instance, we need to update the kube api server arguments in the `/etc/kubernetes/manifests/kube-apiserver.yaml`. Each argument should be on a separate line. Add the necessary flags under the command section to enable OIDC authentication:
```yaml
spec:
  containers:
  - name: kube-apiserver
    command:
    - kube-apiserver
    - --oidc-issuer-url=<OIDC_ISSUER_URL>
    - --oidc-client-id=<YOUR_CLIENT_ID>
    - --oidc-username-prefix=username
```
After making changes to the kube-apiserver configuration, restart the Kubernetes API server for the changes to take effect:
```shell
kubectl get pods -n kube-system -o wide | grep kube-apiserver
```
The latter command will return the name of the <kube-apiserver-pod-name> running. We should restart it by deleting it. It will be recreated automatically.
```shell
kubectl delete pod <kube-apiserver-pod-name> -n kube-system
```
Check the status of the newly created k8s-api pod:
```shell
kubectl get pods -n kube-system -o wide | grep kube-apiserver
```

- Don't forget to update the kubeconfig file as well to add the redirect URI configured with OIDC (same as for Minikube):
```yaml
current-context: kubernetes-admin@kubernetes
apiserver:
  redirect-uri: http://localhost:59785
kind: Config
```
- Roles and Rolebindings will be created upon each user environment creation. Each user will be granted full access to resources but within his/her namespace only. Please check the code for more technical details and comments. The network policies to isolate each ns are explained below,please keep reading. 

- At this point, your Kubernetes cluster is configured to use OIDC for authentication. Test the OIDC authentication by using an OIDC identity from the provider to access the Kubernetes API server. For example:
```shell
kubectl get pods --token=<YOUR_ID_TOKEN>
```
Replace <YOUR_ID_TOKEN> with an ID token issued by the OIDC provider after authenticating with it (can be printed by the backend, just uncomment the instruction).

That's it! Your Kubernetes cluster is now configured with OIDC for user authentication, and users from the OIDC provider should be able to access the cluster using their ID tokens.

#### Namespace isolation: Network policy & roles
When an ID token is passed to k8s API, after verification, we extract the `username` claim from the pauload of decoded token to create k8s resources related to each user. For instance, the first thing to create is the namespace which will be named `username-namespace`. Following the same annotation, other resources like roles and service accounts will be created. Thus, we need to install some sort of seperation of users worksapces and that is achieved by isolation their namepeaces.

Namespace isolation in Kubernetes provides each user with their own isolated environment, ensuring resource separation and enhanced security. For example, if User A has a namespace `user-a-ns` and User B has `user-b-ns`, they can't access each other's resources, preventing interference and unauthorized access.

Benefits:

- Resource Isolation: Pods, services, and deployments are confined within each user's namespace.
- Security: Unauthorized access to other users' resources is blocked.
- Resource Quotas: Each user's namespace can have specific resource limits.
- RBAC Enforcement: Fine-grained permissions can be assigned based on namespaces.
- Logical Organization: Resources are grouped logically for better management.
- Scalability: New users can be easily added without affecting existing ones.

Overall, namespace isolation ensures a safe and organized environment for multiple users to work independently within a shared Kubernetes cluster. In our case, we followed the same philosophy as demistifyed in this [tutorial](https://gitlab.inria.fr/fantastic-fanfare/adorable_alien/-/tree/develop/namespace%20isolation%20and%20RBAC%20hands%20on#isolation-of-namespaces-with-network-policies) to achieve isolation between different users namespace.
In our code, we used **labels** to distinguish between ns and **ingress network policy** to block all incoming traffic to each ns using empty label selector to match all namespaces except the user's namespace. 

After that, we instore the roles definition and roles binding for each service account created upon a user authentication. So technically we are creating service accounts wich represent users in the cluster. That way we deploy roles and bind them to each service account.

#### Time Boudning User Experience:
The playload structure of the __id_token__ looks like the following encoded data related to the identity of the user after authentocating to INRIA account and authorizing OIDC service to use some credentials like email, username:
```json
{
  "iss": "https://account.ilabt.imec.be", // OIDC issuer
  "aud": [
    "<client_id>" // Client_ID provided by OIDC after registration
  ],
  "iat": 1690808014,
  "exp": 1690811614, // Expiration time (1h in this example)
  "auth_time": 1690808013,
  "at_hash": "VGyeCeXigqtNmXA",
  "sub": "jdoe@inria.fr",
  "username": "jdoe", 
  "urn": "urn:publicid:IDN+ilabt.imec.be+user+jdoe",
  "project_urns": [
    "urn:publicid:IDN+ilabt.imec.be+project+jfed-blueprint"
  ],
  "email": "jane.doe@inria.fr",
  "public_ssh_key": "<attributed_public_key>"
}
```
The time granted for tokens to be valid is 1h since issued. Hence we have implemented a concurrent controller/ticker to periodically check the token expiration time and inform the user when only 10 mins are left for the token to be valid. The idea is to prompt the user to ask for his token to be refreshed within OIDC or to end his experience when the token expires. The consequence of the latter case is to erase the user and provoke a clean up of all his workspace (resources). 

- The former case can happen only if resources are available for another 1h slot, which means no user is using that slot or ideally queuing to use it.

The concurrent 'time checker' is ran in a non-blocking manner such that the main program is executed where the go routine is parallelilly checking the token expiration time. The resources are created normally and when 10 mins are left for the token to be invalid, a message will be sent alerting about the matter and prompting the user to type `yes` or `no` to ask for the token refresh mechanism:
```
2023/08/03 10:48:30 The token is valid.
Username: kchiboub
Kubernetes user 'kchiboub' created successfully.
Your token will expire in 10 minutes. Would you like to refresh it? (yes/no)
```
After the expiration of the token, a message will pop up and a clean up of the user k8s envoromenent such that it will be completly erased:
```
Your token has expired. Please retry authentication later. 
BTW, your k8s setup will be detroyed. Next time, please think of refreshing your about-to-expire token if you're still experimenting.
Cleaning up the user workspace...
```
> ##### Token Refresh Mechanism:
> Upon receiving user consent to refresh the token, the application sends a request to the authorization server using the user's refresh token. The server validates the refresh token and issues a new access token along with an updated refresh token if necessary. 
> The application updates the user's session by replacing the expired access token with the new one. This seamless process maintains user authentication without requiring them to log in again, providing uninterrupted access to protected resources.
#### Second test: Create a user in the cluster
Having passed an id-token, generated by our OIDC provider after successful authentication, to the k8s service, it creates a namespace with the username provided in the playload of the token, which in this example `kchiboub`, as well as a network policy, a service account, a role and a role binding. You will see that the validation of the token and if the user environment is created successfully.

We can verify these recources in our cluster using kubectl:  
![image resources](./resources.png)

In the backend logs, you can make sure that everything is working fine and that the go routines are not blocking the program, instead they are proving a parallel operation where tokens expiration are periodically checked, the k8s user environment is created successfully and the browser is loading the list of sim card information logged from the frontend form.  
![image resources2](./allfine.png)