package k8s

import (
	"context"
	"flag"
	"fmt"
	"time"
	"net/http"

	"github.com/golang-jwt/jwt/v4"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	tok "github.com/KaoutarChiboub/Go-eat-kookie/sc-login/token"
)

// Define the kubeconfig flag at the package level. If using Minikube:
//var kubeconfig = flag.String("kubeconfig", "~/.kube/config", "absolute path to the kubeconfig file")
// If using kubeadm: 
var kubeconfig = flag.String("kubeconfig", "/etc/kubernetes/admin.conf", "absolute path to the kubeconfig file")


func K8sSetUp(IdToken string, refreshToken string, w http.ResponseWriter, r *http.Request) {

	// Verify the token first
	if !tok.VerifyToken(IdToken) {
		fmt.Println("Verifying ID token failed. Please try again with a valid token!")
		return
	}

	flag.Parse()
	// Authenticate with the Kubernetes API using the ID token
	clientset, err := authenticateWithKubernetesAPI(IdToken)

	// Parse the ID token to extract the claims (payload)
	token, _, err := new(jwt.Parser).ParseUnverified(IdToken, jwt.MapClaims{})
	if err != nil {
		fmt.Println("Failed to parse ID token:", err)
		return
	}

	// Access the "sub" claim from the token's claims
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		fmt.Println("Failed to extract claims from ID token.")
		return
	}

	// Extract the username from the claims
	username, ok := claims["username"].(string)
	if !ok {
		fmt.Println("Username not found in the token claims.")
		return
	}
	fmt.Println("Username:", username)

	// Create a channel to receive the token expiry notification
	tokenExpiryCh := make(chan bool)
	// Extract the expiration time from the claims and convert it to time.Duration
	exptime := int64(claims["exp"].(float64))
	expiryDuration := time.Unix(exptime,0)
	
	// goroutine to check expiration of the id token
	go tok.CheckTokenExpiry(tokenExpiryCh, expiryDuration)

	// Now you have the Kubernetes client token, you can use it to create the Kubernetes user
	err1 := createUser(clientset, username)
	if err1 != nil {
		fmt.Println("Error creating Kubernetes user:", err1)
		return
	}
	fmt.Printf("Kubernetes user '%s' created successfully.\n", username)

    // Loop to handle token expiry checks and timer ticks
	for {
        select {
        case <-tokenExpiryCh:
            // Notify the user that the token will expire soon.
            fmt.Println("Your token will expire in 10 minutes. Would you like to refresh it? (yes/no)")
			// Get the user's input
			var response string
			fmt.Scanf("%s", &response)
			// If the user wants to refresh the token, redirect them to the appropriate endpoint
			if response == "yes" {
				fmt.Println("Redirecting to the token refresh request...")
				if referr := tok.RefreshTokenHandler(refreshToken, w, r); referr!=nil{
					fmt.Printf("Failed to refresh token: %v", referr)
				}
			}
		case <-time.After(time.Until(expiryDuration)):
			// Token has expired, print an appropriate message and handle token refresh/authentication.
			fmt.Println("Your token has expired. Please retry authentication later.\n Btw, your k8s setup will be detroyed. Next time, please think of refreshing your about-to-expire token if you're still experimenting.")
			// Clean up the user workspace
			fmt.Println("Cleaning up the user workspace...")
			err2 := deleteUserWorkspace(clientset, username)
			if err2 != nil {
				fmt.Println("Error deleting user workspace:", err2)
			}
			return
        }
    }

}

func authenticateWithKubernetesAPI(idToken string) (*kubernetes.Clientset, error){
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	// Set the Bearer token in the configuration
    config.BearerToken = idToken

    // Create a Kubernetes clientset
    clientset, err := kubernetes.NewForConfig(config)
    if err != nil {
        return nil, err
    }
	return clientset, nil
}


func createUser(clientset *kubernetes.Clientset, username string) error {
	// Create a new namespace for the user
	namespace := fmt.Sprintf("%s-namespace", username)
	newNamespace := &corev1.Namespace{
		ObjectMeta: v1.ObjectMeta{
			Name: namespace,
			Labels: map[string]string{
				"user": username, // Add a label "user" with the value as the username
			},
		},
	}

	_, err1 := clientset.CoreV1().Namespaces().Create(context.TODO(), newNamespace, v1.CreateOptions{})
	if err1 != nil {
		return err1
	}

	// Create a new service account for the user.
	srvaccount := fmt.Sprintf("%s-service-account", username)
    serviceAccount := corev1.ServiceAccount{
        ObjectMeta: v1.ObjectMeta{
            Name: srvaccount,
            Namespace: namespace,
        },
    }
    _, err2 := clientset.CoreV1().ServiceAccounts(namespace).Create(context.TODO(), &serviceAccount, v1.CreateOptions{})
    if err2 != nil {
        return err2
    }

	// Create a new Network Policy to block all ingress traffic
	networkPolicy := &netv1.NetworkPolicy{
		ObjectMeta: v1.ObjectMeta{
			Name:      fmt.Sprintf("%s-deny-ingress", username),
			Namespace: namespace,
		},
		Spec: netv1.NetworkPolicySpec{
			PodSelector: v1.LabelSelector{}, // Select all pods within the namespace
			PolicyTypes: []netv1.PolicyType{
				netv1.PolicyTypeIngress, // Block ingress traffic
			},
			Ingress: []netv1.NetworkPolicyIngressRule{
				{
					From: []netv1.NetworkPolicyPeer{
						{
							NamespaceSelector: &v1.LabelSelector{
								MatchLabels: map[string]string{
									"user": username, // Empty label selector will match all namespaces except the user's namespace
								},
							},
						},
					},
				},
			},
		},
	}

	_, errn := clientset.NetworkingV1().NetworkPolicies(namespace).Create(context.TODO(), networkPolicy, v1.CreateOptions{})
	if errn != nil {
		return errn
	}

	// Define the Role for the user within the new namespace
	role := &rbacv1.Role{
		ObjectMeta: v1.ObjectMeta{
			Name:      fmt.Sprintf("%s-role", username),
			Namespace: namespace,
			Labels: map[string]string{
				"user": username,
			},
		},
		Rules: []rbacv1.PolicyRule{
			{
				Verbs:     []string{"create", "update", "delete", "get", "list", "watch"},
				APIGroups: []string{""},
				Resources: []string{"*"},
			},
		},
	}

	_, err3 := clientset.RbacV1().Roles(namespace).Create(context.TODO(), role, v1.CreateOptions{})
	if err3 != nil {
		return err3
	}

	// Create the RoleBinding for the user in the new namespace
	roleBinding := &rbacv1.RoleBinding{
		ObjectMeta: v1.ObjectMeta{
			Name:      fmt.Sprintf("%s-binding", username),
			Namespace: namespace,
		},
		Subjects: []rbacv1.Subject{
			{
				Kind:     "ServiceAccount",
				Name:     srvaccount,
				APIGroup: "",
			},
		},
		RoleRef: rbacv1.RoleRef{
			Kind:     "Role",
			Name:     fmt.Sprintf("%s-role", username),
			APIGroup: "",
		},
	}

	_, err4 := clientset.RbacV1().RoleBindings(namespace).Create(context.TODO(), roleBinding, v1.CreateOptions{})
	if err4 != nil {
		return err4
	}
	return nil
}

func deleteUserWorkspace(clientset *kubernetes.Clientset, username string) error {
	userNamespace := fmt.Sprintf("%s-namespace", username)
	err := clientset.CoreV1().Namespaces().Delete(context.TODO(), userNamespace, v1.DeleteOptions{})
	if err != nil {
		return err
	}
	return nil
}