const userICCID = document.getElementById("user-iccid")
userICCID.setAttribute("required", "")
const userIMSI = document.getElementById("user-imsi")
userIMSI.setAttribute("required", "")
const userLAI = document.getElementById("user-lai")
userLAI.setAttribute("required", "")
const userK = document.getElementById("user-k")
userK.setAttribute("required", "")
const userOEN = document.getElementById("user-oen")
userOEN.setAttribute("required", "")
const userLoginButton = document.getElementById("user-login-button")
const userFetch = document.getElementById("fetch-users")
const userTable = document.getElementById("users-table")


userLoginButton.addEventListener("click", () => {
    console.log("Login in process")
    verifyUser();
})

userFetch.addEventListener("click", () => {
    console.log("Listing all logged-in users...")
    listUsers();
})

// userTable.add

function verifyUser() {
    if (userICCID.value == "" || userIMSI.value == "" || userLAI.value == "" || userK.value == "" || userOEN.value == "") {
        alert("Sim input fields must all be filled out!");
        return false;
    } else {
        let data = {
            "ICCID": userICCID.value,
            "IMSI": userIMSI.value,
            "LAI": userLAI.value,
            "K": userK.value,
            "OEN": userOEN.value
    
        };
        fetch("http://localhost:8008/sim-login", {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
        } 
        }).then((response) => {
            if (response.ok) {
                console.log("YAAY! SIM card information are unique!");
                fetch("http://localhost:8008/authentication", {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json"
                    }
                }).then((response) => {
                    if (response.ok) {
                        // If the response is successful, the user is redirected to the OIDC server for authentication
                        window.location.href = response.url;
                    } else {
                        // Handle error if needed
                        console.log("Failed to initiate authentication.");
                    }
                }).catch((error) => {
                    console.log(error)
                })
            } else {
                response.json().then(data => {
                console.log("SIM card information are not unique: "+ data.massage)
                alert("Ooups... SIM card information are not unique!\n")
                });
            }
        })
        .catch((error) => {
            console.log(error)
        });
    }
}

function listUsers() {
    if (verifyUser()) {
        fetch("http://localhost:8008/simlist", {
             method:"GET",
        }).then((response)=> {
            if (response.ok) {
                return response.json()
            } else {
                throw new Eroor("Failed to fetch user list");
            }
        }).then((data) => {
            console.log("Logged-in users: " + JSON.stringify(data));
            showTitle();
            renderTable(data);
        }).catch((error) => {
            console.log(error);
        });
    } else {
        console.log("User is not logged in or authentication failed");
        alert("Please login and authenticate yourself to ba able to continue your query!")
    }
}

function renderTable(data) {
    // Clear previous table data
     userTable.innerHTML = "";

    // Create table header
    const tableHeader = document.createElement("thead");
    const headerRow = document.createElement("tr");
    const headers = ["ID", "ICCID", "IMSI", "LAI", "K", "OEN"];
    headers.forEach((headerText) => {
      const headerCell = document.createElement("th");
      headerCell.textContent = headerText;
      headerRow.appendChild(headerCell);
    });
    tableHeader.appendChild(headerRow);
    userTable.appendChild(tableHeader);
  
    // Create table body
    const tableBody = document.createElement("tbody");
    data.forEach((user) => {
      const row = document.createElement("tr");
      Object.values(user).forEach((value) => {
        const cell = document.createElement("td");
        cell.textContent = value;
        row.appendChild(cell);
      });
      tableBody.appendChild(row);
    });
    userTable.appendChild(tableBody);
}

function showTitle(){
    // Create table title
    const caption = document.getElementById("table-caption");
    caption.style.display = "table-caption";
}