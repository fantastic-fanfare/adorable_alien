package main

import (
	"database/sql"  // To connect to sql db
	"encoding/json" // To encode data to json
	"fmt"           // To print out information about servers and connections
	"log"           // To log out errors and msgs
	"net/http"  
	"net/url"
	"os"
	"io/ioutil"
	"strings"
	
	"github.com/joho/godotenv"
	"github.com/gorilla/mux"
	"github.com/netbox-community/go-netbox/v3/netbox"
	_ "github.com/lib/pq"
	"github.com/rs/cors"
	tok "github.com/KaoutarChiboub/Go-eat-kookie/sc-login/token"
	net "github.com/KaoutarChiboub/Go-eat-kookie/sc-login/netbox"
	k8s "github.com/KaoutarChiboub/Go-eat-kookie/sc-login/k8s"
)

type User struct {
	ID    string `json:"id"`
	ICCID string `json:"ICCID"`
	IMSI  string `json:"IMSI"`
	LAI   string `json:"LAI"`
	K     string `json:"K"`
	OEN   string `json:"OEN"`
}

var db *sql.DB
var (
	redirectURI = "http://localhost:8008/redirect-call"
	tokenURL = "https://portal.slices-sc.eu/oauth/token"
	authoURL = "https://portal.slices-sc.eu/oauth/authorize"
)
// Where to parse the http response body
var Token struct {
	AccessToken  string `json:"access_token"`
	ExpiresIn    int    `json:"expires_in"`
	IDToken string `json:"id_token"`
	RefreshToken string `json:"refresh_token"`
	Scope string `json:"scope"`
	TokenType    string `json:"token_type"`
}

// The following functions are used to handle HTTP requests
func getSimList(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		rows, err := db.Query("SELECT * FROM users")
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()

		var users []User
		for rows.Next() {
			var u User
			if err := rows.Scan(&u.ID, &u.ICCID, &u.IMSI, &u.LAI, &u.K, &u.OEN); err != nil {
				log.Fatal(err)
			}
			if err := rows.Err(); err != nil {
				log.Fatal(err)
			}
			users = append(users, u)
		}
		res, err := json.Marshal(users)
		if err != nil {
			log.Fatal(err)
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(res)
	}
}

func createSimUser(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var u User
		w.Header().Set("Content-Type", "application/json")
		json.NewDecoder(r.Body).Decode(&u)
		if isSIMunique(db, u) {                                                                                                        
			_, err := db.Exec("INSERT INTO users (ICCID, IMSI, LAI, K, OEN) VALUES ($1, $2, $3, $4, $5)", u.ICCID, u.IMSI, u.LAI, u.K, u.OEN)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			response := struct {
				Message string `json:"message"`
			}{
				Message: "SIM card information are not unique",
			}
			w.WriteHeader(http.StatusForbidden)
			json.NewEncoder(w).Encode(response)
			return
		}
		json.NewEncoder(w).Encode(u)
	}
}
func isSIMunique (db *sql.DB,u User) bool {
	query := "SELECT EXISTS(SELECT 1 FROM users WHERE ICCID = $1 AND IMSI = $2 AND LAI = $3 AND K = $4 AND OEN = $5)"
	var exists bool
	err := db.QueryRow(query, u.ICCID, u.IMSI, u.LAI, u.K, u.OEN).Scan(&exists)
	if err != nil {
		log.Fatal(err)
	}

	// If the result is false (not exists), it means the SIM card information is unique
	return !exists
}

func authUser(w http.ResponseWriter, r *http.Request) {
	redirectURL := fmt.Sprintf("%s?client_id=n5qsyXbYTeI89uVG3XLmYr2r&redirect_uri=%s&scope=openid userinfo&response_type=code&state=013664b1596e6303bb64b951746e7e09197671c7",
		authoURL, redirectURI)
	http.Redirect(w, r, redirectURL, http.StatusFound)
}

func callback(w http.ResponseWriter, r *http.Request) {
	// Extract the authorization code from the query parameters
	code := r.URL.Query().Get("code")
	if code == "" {
		http.Error(w, "Authorization code is missing", http.StatusBadRequest)
		return
	}
	// Exchange the authorization code for access tokens
	// Prepare the request body with required parameters
	data := url.Values{}
	data.Set("grant_type", "authorization_code")
	data.Set("code", code)
	data.Set("redirect_uri", redirectURI)
	data.Set("client_id", "n5qsyXbYTeI89uVG3XLmYr2r")
	data.Set("client_secret", "1xQOIJg60FOWNeFoHgO6jDkP1PkldEn0aggAmbB1gWWIAF0a")

	// Send a POST request to the token endpoint to exchange the code for tokens
	req, err := http.NewRequest("POST", "https://portal.slices-sc.eu/oauth/token", strings.NewReader(data.Encode()))
	// Set the content type header for the request
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	// Create an HTTP client
	client := &http.Client{}
	// Send the request
	resp, err := client.Do(req)
	if err != nil {
		http.Error(w, "Failed to exchange code for tokens", http.StatusInternalServerError)
		return
	}
	
	defer resp.Body.Close()

	// Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, "Failed to read response body", http.StatusInternalServerError)
		return
	}
	//fmt.Println(body)
	// Check if the response contains an error
    var errResponse struct {
        Error string `json:"error"`
    }

    if err := json.Unmarshal(body, &errResponse); err == nil && errResponse.Error != "" {
        http.Error(w, "Token exchange error: "+errResponse.Error, http.StatusInternalServerError)
        return
    }

	err = json.Unmarshal(body, &Token)
	if err != nil {
		http.Error(w, "Failed to parse token response", http.StatusInternalServerError)
		return
	}

	if !tok.VerifyToken(Token.IDToken) {
		http.Error(w, "Token is not valid after JWKS verification.", http.StatusInternalServerError)
		return
	} else {
		fmt.Printf("The token is valid.\n")
	}
	// Store the access token and other relevant information as needed in a cookie to track user authentication for later use
	http.SetCookie(w, &http.Cookie{
        Name:     "token",
    	Value:    Token.IDToken,
    	HttpOnly: true,
    	Secure:   true,
    })

	// Non blocking go routine to create k8s workspace for each authenticated user by calling the k8s service
	go k8s.K8sSetUp(Token.IDToken, Token.RefreshToken, w, r)

	// Redirect the user to the desired page after successful authentication (k8s setup parallely executed)
	http.Redirect(w, r, "/simlist", http.StatusFound)
}


// Middleware wthat will check if the user has a valid token to access some endpoints like netbox and k8s services, if not an authentication procedure will occur
func requireAuth(next http.HandlerFunc) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
		tokenCookie, err := r.Cookie("token")
        if err != nil || !tok.VerifyToken(tokenCookie.Value) {
        	// Redirect the user to the authentication endpoint if not authenticated.
        	http.Redirect(w, r, "/authentication", http.StatusFound)
        	return
        }
		// Call the next handler if the user is authenticated.
		next.ServeHTTP(w, r)
	}
}

func main() {
	godotenv.Load()
	netboxToken := os.Getenv("NETBOX_TOKEN")
	netboxHost := os.Getenv("NETBOX_HOST")

	//Connection to postgres database
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatalf("Could not connect to database: %v", err)
	}
	defer db.Close()

	if db.Ping() != nil {
		log.Fatalf("Failed to ping the databse!\n Error: %v", db.Ping())
		return
	}

	//Creating netbox client to communicate with NetBox API server using api-token and host name stored in .env. We thus created a transport connection for our client.
	if netboxToken == "" {
		log.Fatalf("Please provide netbox API token via env var NETBOX_TOKEN")
	}
	if netboxHost == "" {
		log.Fatalf("Please provide netbox host via env var NETBOX_HOST")
	}
	c := netbox.NewNetboxWithAPIKey(netboxHost, netboxToken)

	// Routing and endpoints
	r := mux.NewRouter()
	r.HandleFunc("/simlist", requireAuth(getSimList(db))).Methods("GET")
	r.HandleFunc("/sim-login", createSimUser(db)).Methods("POST")
	r.HandleFunc("/netbox/list-device",requireAuth(net.GetDevices(c))).Methods("GET")
	r.HandleFunc("/authentication", authUser).Methods("GET")
	r.HandleFunc("/redirect-call", callback)
	r.HandleFunc("/netbox/dcim/cables/{id}", requireAuth(net.UpdateCable(c))).Methods("PUT")

	handler := cors.Default().Handler(r)

	log.Printf("Starting HTTP server on port 8008:\n")
	log.Fatal(http.ListenAndServe(":8008", handler))

}
