package token

import(
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"encoding/json"
	"io/ioutil"
)

// Where to parse the http response body
var Token struct {
	AccessToken  string `json:"access_token"`
	ExpiresIn    int    `json:"expires_in"`
	IDToken string `json:"id_token"`
	RefreshToken string `json:"refresh_token"`
	Scope string `json:"scope"`
	TokenType    string `json:"token_type"`
}

// Refresh token mechanism: ask for a new id-token
func RefreshTokenHandler(refreshToken string, w http.ResponseWriter, r *http.Request ) error {
	// Prepare the request body with required parameters
	data := url.Values{}
	data.Set("grant_type", "refresh_token")
	data.Set("refresh_token", refreshToken)
	data.Set("client_id", "n5qsyXbYTeI89uVG3XLmYr2r")
	data.Set("client_secret", "1xQOIJg60FOWNeFoHgO6jDkP1PkldEn0aggAmbB1gWWIAF0a")

	req, err := http.NewRequest("POST", "https://portal.slices-sc.eu/oauth/token", strings.NewReader(data.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Failed to refresh token!")
		return err
	}
	
	defer resp.Body.Close()

	// Read the response body
	body, err1 := ioutil.ReadAll(resp.Body)
	if err1 != nil {
		fmt.Printf("Failed to read response body")
		return err1
	}
	// Check if the response contains an error
	var errResponse struct {
		Error string `json:"error"`
	}

	if err2 := json.Unmarshal(body, &errResponse); err2 == nil && errResponse.Error != "" {
		fmt.Printf("Token refresh error: "+errResponse.Error)
		return err2
	}
	err = json.Unmarshal(body, &Token)
	if err != nil {
		fmt.Printf("Failed to parse token response")
		return err
	}

	// Update the Id token and other relevant information as needed in a cookie to keep user authentication
	http.SetCookie(w, &http.Cookie{
		Name:     "token",
		Value:    Token.IDToken,
		HttpOnly: true,
		Secure:   true,
	})
	return nil
}