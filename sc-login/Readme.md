## Sim Card login: Frontend & Backend
This section is dedicated for describing the philosophy/structure of a frontend with HTML, CSS and JavaScript that is connected to a Go REST backend in order to provide a user sim login with authentication via an OIDC provider (ilab/slices): 
1) Insert user sim credentials from the frontend & stored in a postgres db.
2) Authenticate through OIDC/OAuth2 with authorization grant.
3) Access k8s and netbox services after successful authentication.
4) List all users data from sim users db.
   
The setup is to authenticate users by redirecting them to OIDC server of SCLICES in order for them to be able to further query resources (k8s environment and netbox resource catalog) from our backend. 

You can clone the repo or the any piece of code provieded here. This documentation will not go deeper into the steps of building the frontend and backend given that it each service is explained separatly in subdirectories. Thers's a frindly beginner [hands-on](https://gitlab.inria.fr/fantastic-fanfare/adorable_alien/-/tree/kooki/REST_API_Go#hands-on-tutorial-develop-rest-api-in-go-with-mux-postgresql-and-docker) for how to build a REST API in Go with Postgres.

- However in this doc, we will discuss some issues faced related, for instance, to resource sharing between servers/apps while testing our requests. Then we will elucidate and talk about the Backend-For-Frontend (BFF) strategy for authorization and authentication with SCLICES OIDC server.

- Finally we will talk about the modeling of our application in a global architecture scope. Stateless or stateful?

### 1- Simple Go REST API with CORS handling:
#### Resource Security Issue
When building our REST API as simple as for usual cases, we countered an issue with network requests that indicates a failure in fetching data from the server and sending back to our backend. The URL we are trying to access, for instance, is `http://localhost:8008/sim-login` which is an endpoint for a POST request where the user logs in his sim card credentials to be stored next in our postgres db.   
Here are some screenshots from our console after trying to insert a user sim credentials from js frontend.  
- Even if the request itself is successful, the latter is preventing from completing because of a network access error.
![console err](./Documentation/conErr.png)

- We thus got no response back from our backend because we didn't configure a proper resource access.
![response err](./Documentation/netErr.png)

Making requests from different **origins** (protocol, port and domain) will cause the browser to block the one that is not similar from which is was served. Browsers enforce the **Same-Origin Policy**, which restricts cross-origin requests by default for security reasons. 

#### Solution: CORS Headers
**CORS** (Cross-Origin Resource Sharing) is a web browser based mechanism that allows not only one but different origins (frontend) to access resources and interact with it from the backend. Click here for more details about [Golang CORS handling](https://www.stackhawk.com/blog/golang-cors-guide-what-it-is-and-how-to-enable-it/).

So getting back to our issue, as you could see in the screenshots above, the request is not successfully working. Why?

The answer is, as you probably suspect in the code provided, the same-origin policy. The Golang app runs on port 8008. However, the client JS app runs on port 5500. As you've seen, **simply having different port numbers is already enough for two URLs to be considered different origins**.

Hence, we will need to define a CORS header `Access-Control-Allow-Origin` to specify which origins are allowed to access the resource.

For that, we will first import Go standard library for CORS which is `rs/cors`. Then with `mux`, configure the CORS header with speficied options as follows:
```go
r := mux.NewRouter()

c := cors.New(cors.Options{
 AllowedOrigins: []string{"http://localhost:5500"},
})
handler := c.Handler(r)
```

Or by the default options which includes allowing requests from any origin (Access-Control-Allow-Origin: *), allowing common HTTP methods, and handling CORS preflight requests. We will use the latter method for our case. Here's a code snippet of how it will look like:
```go
r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte("{\"hello\": \"world\"}"))
	})

	// Use default options
	handler := cors.Default().Handler(r)
	http.ListenAndServe(":8008", handler)
```

After updating our code, the problem should be solved as we can see in the broswer the following results:
![console yes](./Documentation/conyes.png)
![net err](./Documentation/netyes.png)

### 2- BFF in a nutshell

#### First, an overview of OAuth2.0/OIDC

OAuth is a sort of “protocol of protocols” or “meta protocol,” meaning that it provides a useful starting point for other protocols (e.g., [OpenID Connect](https://openid.net/specs/openid-connect-core-1_0.html), [NAPS](https://openid.net/wg/napps/), and [UMA](https://docs.kantarainitiative.org/uma/draft-uma-core.html)). It garantees authorization only to access data/resources from another server/app passing by an authorization server that generates Client ID and Secret to be recognized by the resource server in order to send an access token + refresh token to read data from it (instead of passing real credentials). It gives the client a key that is useful but doesnt tell anything about the client.

Here comes OIDC on top of OAuth to give the client a badge providing basic info about who they are and establishing a login session. When an authorization server supports OIDC, its called Identity Provider. With OIDC, when the client sends an authorization code, client id and secret, the resource server sends NOT ONLY an access token but also an ID token ( must be JWT) containing claims. OpenID Connect provides user authentication and single sign-on (SSO) functionality when the server hosts multiple apps that needs access to resource.

Echanging access code with token is of security matters. In the front channel between the client and authentication server (AS), the chance to sniff or do malicious intervention is eventual. That’s why we use access token (that contains end user data) in the back channel server to server.

With Slices, using OIDC identity provider to represent the AS and RS at the same time will help clients apps in the following procedure:  
• Ask Slices Portal to access SC testbed account from OIDC AS request.  
• Exchange code to access tokens by requesting it from AS (to the token endpoint).  
• Validate ID token in SC resource server to grant access to data based on authenticated users.  

#### Now, how is it integrated in our development?

Let's put it as simple and raw as possible:
- The normal sign up from [SCLICES portal](https://portal.slices-sc.eu/login): Select institute —> Create a user account + terms and conditions —> Create or join a project —> Wait for approval.
__Because SCLICES lightweight federation has openly provided their [OIDC metadata](https://portal.slices-sc.eu/.well-known/openid-configuration), we can test and try it in OIDC playground to grasp the flow and callback exchanges between the latter and SCLICES testbed portal.__

- [OIDC playground](https://openidconnect.net/#): Custom the server by using discovery doc url, client ID and client secret. Save the config and start redirecting to OIDC (request) => Redirect to sclices portal to grant authorization (to OIDC) to access SC testbed account => An access code is generated as a reponse and should be turned into an access token by having OIDC server make a request (`POST … &code=<access_code>`) to our token endpoint => json reponse `{access token/expire/id-token/scope/type..}` => Verify that the ID Token sent was from the correct place by validating the JWT’s signature with JWKS URI provided in the metadata.

We implemented the latter scenario in our code to have the following tapesery:
- **Backend:** Implement the authentication flow by handling the redirect from the frontend to the OIDC authorization endpoint. Once the user is authenticated and redirected back to our specified redirect URL, we can exchange the authorization code for an access token using the appropriate OIDC server's token endpoint. Validate the received access token using the JWKS obtained from the JWKS URI.
-  **Frontend:** Initiate the authentication flow by redirecting the user to the OIDC server's authorization endpoint after trying to login via the frontend interface. Pass the necessary parameters (client ID, scope, redirect URL, etc.) in the request to this endpoint. Once the user is authenticated in SCLICES testbed portal, the backend will take charge in redirecting the user to the frontend interface in a logged-in mode. Then the backend will generate a token or a session or even a cookie for the user to be able to query resources as long as the latter is valid and not expired.

### 3- Statefull or stateless architecture?
At a glance, a stateless backend might seem straightforward – it doesn't store user data, sessions, or tokens, making it ideal for efficient scaling. However, many applications require data persistence, and that's where stateful components enter the picture. Consider our scenario where the backend is responsible for handling authentication, authorization and routing, while a connected database (Postgres) stores user SIM card inputs received from the frontend.

This database introduces a stateful element into the architecture. Even though the backend doesn't store tokens or session data, the database's statefulness means the overall application architecture is stateful.

> * * * * * * * * * * * * * * *
> ##### How to Implement Stateful Backend Architecture with Kubernetes Pods? Here are few steps to consider:
> **Stateless Backend Pods**:
> Deploy our stateless backend code within Kubernetes pod. This pod handles authentication, authorization, and communication with the frontend. Replicate this pod to ensure high availability and efficient load balancing.
> 
> **Stateful Database Pods**:
> For the connected database, opt for Kubernetes `StatefulSets`. StatefulSets offer features like stable network identities, persistent storage, and ordered pod deployment. This ensures data persistence and allows for dynamic scaling of database pods.
> 
> **Communication and Services**:
> Configure Kubernetes Services to expose stable network endpoints for both the backend and database pods. These Services act as load balancers, facilitating communication between the frontend and backend while directing traffic to the appropriate database pod.
> 
> **Environment Configuration**:
> Configure environment variables or connection strings in the backend pods to enable communication with the stateful database pods. This ensures that the backend can seamlessly interact with the database.
> 
> **Persistent Volumes**:
> Leverage Kubernetes persistent volumes to store data for your stateful database pods. This data persists even during pod restarts or rescheduling, safeguarding against data loss.
> 
> **Pod Affinity/Anti-Affinity**:
> Use Kubernetes pod affinity and anti-affinity rules to ensure that backend pods are scheduled on different nodes than database pods. This prevents resource contention and enhances performance.
