package netbox

import (
	"fmt"
	"encoding/json"
	"net/http"
	"strconv"
	"log"

	"github.com/gorilla/mux"
	"github.com/netbox-community/go-netbox/v3/netbox/client"
	"github.com/netbox-community/go-netbox/v3/netbox/client/dcim"
	"github.com/netbox-community/go-netbox/v3/netbox/models"
)

func GetDevices(c *client.NetBoxAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := dcim.NewDcimDevicesListParams()
		res, err := c.Dcim.DcimDevicesList(req, nil)
		if err != nil {
			log.Fatal(err)
			fmt.Printf("Cannot get Devices list: %v\n", err)
		}
		for _, dev := range res.Payload.Results {
			json.NewEncoder(w).Encode(map[string]interface{}{
				"ID":     dev.ID,
				"name":   dev.Name,
				"type":   dev.DeviceType.Display,
				"status": dev.Status.Value,
				"site":   dev.Site.Name,
				// You can add as many properties to display as you want here
			})
		}
	}
}

func UpdateCable(c *client.NetBoxAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		cabID, _ := strconv.Atoi(params["id"])
		var body models.WritableCable
		json.NewDecoder(r.Body).Decode(&body)
		// Get the current cable object to preserve A and B terminations
		getReq := dcim.NewDcimCablesReadParams().WithID(int64(cabID))
		getRes, err := c.Dcim.DcimCablesRead(getReq, nil)
		if err != nil {
			fmt.Printf("Cannot get cable / Does not exist probably!: %v\n", err)
			return
		}
		curCable := getRes.Payload
		req := dcim.NewDcimCablesUpdateParams().WithID(int64(cabID))
		req.SetData(&models.WritableCable{Status: body.Status, ATerminations: curCable.ATerminations, BTerminations: curCable.BTerminations})
		_, errr := c.Dcim.DcimCablesUpdate(req, nil)
		if errr != nil {
			log.Fatal(errr)
			fmt.Printf("Cannot update cable: %v\n", errr)
		}
		fmt.Printf("Device disconnected/connected successfully!")
		json.NewEncoder(w).Encode("Cable successfully updated :D! You can visualize changes in your NetBox dashboard")

	}
}