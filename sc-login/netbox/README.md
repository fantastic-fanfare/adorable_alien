# Configuring NetBox (Resource Catalog) service in a Go backend

Please check the demystified documentation around setting up a NetBox client between the NetBox instance and the backend. [Here](https://gitlab.inria.fr/fantastic-fanfare/adorable_alien/-/tree/develop/Netbox-tests#hands-on-2-go-rest-api-with-netbox-docker-api) you can also find practical examples showing how to handle some requests.

The idea in the overal backend application is that only authenticated users can send requests to query NetBox service. If not authenticated, users will be redirected to the authorization and authentication mechanism.

Make sure to adjust netbox host variable to the appropriate host. In our case where a tunnel was installed bewteen a server that was providing netbox traffic and the localhost on port 8000, we had to change `NETBOX_HOST = "host.docker.internal:8000"` in `.env` instead of `"localhost:8000"` to make the docker container access the actual localhost.