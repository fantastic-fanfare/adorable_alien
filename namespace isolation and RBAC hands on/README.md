# Isolation of namespaces with Kubeadm and Calico via Network Policies
![Calico Logo](https://www.dasblinkenlichten.com/wp-content/uploads/2017/05/Calico_Logo_Larger_Text.png)
In this hands on we will be using Kubernetes NetworkPoliciesNetwork. But before that we need to use a network solution which will support networkpolicies since creating a NetworkPolicy resource without a controller that implements it will have no effect. For that we need a container network interface (CNI) plugin like Calico that will allow us to set up network policies and enforce them within a kubernetes cluster to control the traffic flow between different pods and namespaces. 
In what follows we try to define rules that specify which pods can communicate with each other and which traffic is allowed or denied. Calico in this case integrates seamlessly with Kubernetes and provides the necessary functionality and create a configurable network between nodes.


## Set up Calico on Minikube 

Before getting First we show how to configure Calico on Minikube. Minikube is a lightweight Kubernetes implementation that creates a VM on your local machine and deploys a simple cluster containing only one node.
before setting calico up, make sure that you have a minikube cluster ready by referring to the [official documentation of Minikube](https://minikube.sigs.k8s.io/docs/start/). To set up calico on your cluster, we can use the build in calico 
```
minikube start --network-plugin=cni --cni=calico
```
However you should consider using Manifest or Operator approach for newer versions, especially that there are issues with v3.20.0 of Calico that was installed in my case.
Instead start your minikube cluster with one control plane node using the following command.
```
minikube start --network-plugin=cni
```
Then we can install Calico on the cluster by simply applying the official manifest for Calico
```
kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.0/manifests/calico.yaml
```
Later on we will install Calico in our Kubeadm cluster using helm.
You can verify Calico installation in your cluster by issuing the following command.

```
watch kubectl get pods -l k8s-app=calico-node -A
```
Make sure that the calico pod is up and running. 
Using Minikube with Calico have some limitations. In what follows we will set up a cluster with Kubeadm instead.
## Creating Kubeadm  cluster with ansible
### Preparing  VMs
We have access to three VMs, we will create in what follows a cluster with one master node and 2 worker nodes.
We first have to set up the environment, so we start by adding a container runtime, in our case Docker Engine (using cri-dockerd), check the [Docker documentation](https://docs.docker.com/engine/install/) for that. Follow through with the installation of kubectl, kubelet and kubeadm, refer to [Kubernetes official Docs](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/). Note that if multiple or no container runtimes are detected kubeadm will throw an error and will request that you specify which one you want to use.
### Set up the cluster
So after initializing the control plane using the following command.


```
sudo kubeadm init --pod-network-cidr=192.168.0.0/16
```
> **Note:** if 192.168.0.0/16 is already in use within your network you must select a different pod network CIDR.

> **Error:** If you find this error: Found multiple CRI endpoints on the host. Please define which one do you wish to use by setting the 'criSocket' field in the kubeadm configuration file: unix: ///var/run /containerd/containerd.sock, unix :///var/run/cri-dockerd.sock, `sudo kubeadm init --pod-network-cidr=192.168.0.0/16 --cri-socket=unix:///var/run/cri-dockerd.sock` will solve the issue.

Then execute the following to configure kubectl (also returned by kubeadm init).

```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
### Join nodes 
After doing this a kubeadm join command will be returned with a token
```
kubeadm join 10.92.171.146:6443 --token qk9z4l.i5tl6weq0p0pze6o
```
This command we can run it in worker node to join it in control plane node (existing k8s cluster) The token serves as an authentication credential for the worker node to join the cluster. If you did not save it, don't worry, you can always create another token
```
kubeadm token generate
kubeadm token create <generated-token> --print-join-command

```
We will automate this with ansible later on.
### Install calico with helm 
First you need to [install helm](https://helm.sh/docs/intro/install/) if that is not already the case.
Add the Calico helm repo:
```
helm repo add projectcalico https://docs.tigera.io/calico/charts
```
Create the tigera-operator namespace.

```
kubectl create namespace tigera-operator
```
Then Install the Tigera Calico operator and custom resource definitions using the Helm chart:

```
helm install calico projectcalico/tigera-operator --version v3.26.0 --namespace tigera-operator
```
Confirm that everything is working as expected similiar to what we did in Minikube section.
### Automation with ansible
It is tiresome to repeat the whole setup when we want to join a node so we wrote some playbooks to automate the configuration of nodes. If you have VMs ready just specify their IP adresses in the hosts file, for example: 
```yml
[masters]
 localhost ansible_connection=local

[workers]
 worker1 ansible_host=10.92.171.79 ansible_user=root
 worker2 ansible_host=10.92.171.22 ansible_user=ubuntu
[all:vars]
 ansible_python_interpreter=/usr/bin/python3`
```
In our case the master node and deployment node are the same, hence the specification of localhost.
First, run the playbook that will install all the dependencies:
```
ansible-playbook -i hosts kube-dependencies.yml
```
Then run the playbook to join the nodes:
```
ansible-playbook -i hosts workers.yml
```

Your cluster is now up and running, and by default, kubeadm sets up your cluster to use and enforce use of RBAC (role based access control). 
We can if we want then label a worker node as follows:
```
kubectl label node lavish-skater node-role.kubernetes.io/worker=worker
```

> **Note:** By default, your cluster will not schedule Pods on the control plane nodes for security reasons. If you want to be able to schedule Pods on the control plane nodes, for example for a single machine Kubernetes cluster, run:
`kubectl taint nodes --all node-role.kubernetes.io/control-plane-`

Your cluster should look like this: 

![Cluster nodes](./cluster_nodes.png)


## Isolation with Network Policies
### Introduction to Network policies
Network plugins in Kubernetes provide a way to extend and customize the networking capabilities of the Kubernetes cluster beyond the default networking model.
Calico will apply the configurations of a NetworkPolicy resource. 
There are two sorts of isolation for a pod: isolation for egress, and isolation for ingress. They concern what connections may be established.
By default, a pod is non-isolated for egress; all outbound connections are allowed. A pod is isolated for egress if there is any NetworkPolicy that both selects the pod and has "Egress" in its policyTypes; we say that such a policy applies to the pod for egress. 
Similarily, a pod is non-isolated for ingress by default; meaning that all inbound connections are allowed. A pod is isolated for ingress if there is any NetworkPolicy that both selects the pod and has "Ingress" in its policyTypes; we say that such a policy applies to the pod for ingress.
### Basic example of Default deny all ingress traffic
In this example we refer to the one given by Calico's documentation since it demonstrates well the application of networkpolicies.

#### default behaviour

Create some nginx pods in the namespace of your choice.

```
kubectl create deployment -n netdemo nginx --image=nginx
```
Expose the deployment through a service.
```
kubectl expose -n netdemo deployment nginx --port=80
```
Ensure the nginx service is accessible.
```
kubectl run -n netdemo access --rm -ti --image busybox /bin/sh
```
This should open a shell, do a curl to the nginx service at port 80, example:
```
curl 10.105.126.178:80
```
You should be able to see somehing like this: 

![nginxcurl](./nginx_curl.png)

#### Using networkpolicies
We can turn on isolation in our namespace. Calico will then prevent connections to pods in this namespace. 
We will create a network policy access-nginx with the following contents:
```ini
kubectl create -f - <<EOF
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: access-nginx
  namespace: netdemo
spec:
  podSelector:
    matchLabels:
      app: nginx
  ingress:
    - from:
      - podSelector:
          matchLabels:
            run: access
EOF

```

This will deny all ingress traffic except from incoming connections from the ''access'' pod.
should now be able to access the service from the access pod.

```
kubectl run -n netdemo access --rm -ti --image busybox /bin/sh
```
but not from another pod, try:
```
kubectl run -n netdemo noaccess --rm -ti --image busybox /bin/sh
```
We get an operation time out

### Isolation of namespaces with Network policies
The goal here is to try and isolate two namespaces, a pod within a namespace in which we apply a certain network policy should not be able to reach another pod in another namespace except from a pod from the same namespace. By default a pod can reach every other pods in the pod network, this is why we need the right network policy to limit this behaviour.

1. Create namespaces and label them

We will create two namespaces, Frodo and Sam and we will label them tenant 1 and tenant 2 respectively 

```
kubectl create namespace frodo
kubectl create namespace sam

```
```
kubectl label namespace frodo tenant=1
kubectl label namespace sam tenant=2

```
2. Create  pods with the images of your choice in the two namespaces

Let us verify that the default behaviour of pods in the same network with a ping from Frodo's pod to Sam's pod

```
kubectl exec -it nginx-frodo -n frodo -- bash
```

> **Note:** You can get the pods IP @ with -o wide option 

The ping to sam's pod should work as expected.

3. Create network policies

Add a yaml file with the following specifications
```yml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: frodonetpolicy
  namespace: frodo
spec:
  podSelector: {}
  policyTypes:
  - Ingress
  ingress:
  - from:
    - namespaceSelector:
        matchLabels:
          tenant: "1"
status: {}
---

apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: samnetpolicy
  namespace: sam
spec:
  podSelector: {}
  policyTypes:
  - Ingress
  ingress:
  - from:
    - namespaceSelector:
        matchLabels:
          tenant: "2"
status: {}

```

> **Note:** Don't forget to do `kubectl apply -f nameofyourfile.yml`

Basically, we block all ingress traffic to the namespace, except if the traffic is coming from a namesapce with the same label, which will still allow traffic from the same namespace.
If you do a ping now or a curl frmo frodo to sam , you'll get an operation time out, unless it is to a pod in the same namespace or another namespace in which we did not configure the network policy, but still traffic from all other namesapces will be blocked.

A next step would be to explore RBAC permissions and user managment in kubernetes to fully make use of network polocies. We can also specify resource quotas to limit resources consumption in a namespace.

