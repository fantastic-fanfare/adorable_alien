# Introduction to RBAC, service accounts and user authentication in Kubernetes
## Authenticating

All Kubernetes clusters have two categories of users: service accounts managed by Kubernetes, and normal users. It is important to note that k8s does not manage users natively; there is no resource for users but they can be indirectly created by importing a list of users into the cluster (static token file) or generating a client certificate for k8s api server and the user of that certificate will be registered as a user in k8s. Normal users cannot be added to a cluster through an API call. We will focus on certificate based access and token based access with service accounts.

API requests are tied to either a normal user or a service account, or are treated as anonymous requests. This means every process inside or outside the cluster, from a human user typing kubectl on a workstation, to kubelets on nodes, to members of the control plane, must authenticate when making requests to the API server, or be treated as an anonymous user.

### Certificate based access

Even though a normal user cannot be added via an API call, any user that presents a valid certificate signed by the cluster's certificate authority (CA) is considered authenticated. In this configuration, Kubernetes determines the username from the common name field in the 'subject' of the cert (e.g., "/CN=bob"). From there, the role based access control (RBAC) sub-system would determine whether the user is authorized to perform a specific operation on a resource.For more details, refer to [Kubernetes Documentation](https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/#normal-user). We will go back with an example of how to do this later on.
### Authenticating with service accounts
service accounts are users managed by the Kubernetes API. They are especially meant for	non human users,  like Jenkins or another tool. Service accounts uses token to authenticate with API server. They are bound to specific namespaces, and created automatically by the API server or manually through API calls. Service accounts are tied to a set of credentials stored as Secrets, which are mounted into pods allowing in-cluster processes to talk to the Kubernetes API. To access k8s api-server we need authentication token, and you should know that Kubernetes automatically sets that value of spec.serviceAccountName present in all pods to default if you don't specify it when you create a Pod.

> **Note:** secrets are not created automatically anymore. You can use kubectl create token to create a token for a service account. Check more [details here](https://kubernetes.io/docs/concepts/configuration/secret/#service-account-token-secrets)
## Authorization
After the request is authenticated as coming from a specific user, the request must be authorized. For that we need to have proper authorization to do some API calls or to limit others.

Kubernetes authorizes API requests using the API server. It evaluates all of the request attributes against all policies and allows or denies the request. All parts of an API request must be allowed by some policy in order to proceed. This means that permissions are denied by default.

### Authorization Modes
The Kubernetes API server may authorize a request using one of several authorization modes:
#### Node
A special-purpose authorization mode that grants permissions to kubelets based on the pods they are scheduled to run. [See node authorization](https://kubernetes.io/docs/reference/access-authn-authz/node/)
#### ABAC
Attribute-based access control (ABAC) defines an access control paradigm whereby access rights are granted to users through the use of policies which combine attributes together. The policies can use any type of attributes (user attributes, resource attributes, object, environment attributes, etc). [See ABAC mode](https://kubernetes.io/docs/reference/access-authn-authz/abac/)
#### RBAC
In this hands on we focus specifically on RBAC permissions. RBAC policies are defined using roles, which are a set of permissions, and are assigned to users or groups.

> The primary difference between ABAC and RBAC is that ABAC evaluates policies based on user attributes, while RBAC evaluates policies based on user roles. o	In general RBAC is the recommended auth mode in k8S , however ABAC can be a good choice for more fine grained access control.

Kubeadm enables the use of RBAC by default, but to enable,  start the apiserver with --authorization-mode=RBAC.

When specified RBAC (Role-Based Access Control) uses the rbac.authorization.k8s.io API group to drive authorization decisions, allowing admins to dynamically configure permission policies through the Kubernetes API.

## Authenticating users with certificates and RBACs
### Create user certificates

We will try to create a user 'sauron', the idea here is to give him limited access to resources and prevent him from using frodo and sam namesapces.
First we generate a private key for our user
```
openssl genrsa -out sauron.key 2048
```
Then we will have to generate a CSR (Certificate signer request) using the command below and the private key you just created. You will prompted to give additional infos, make sure to set CN and O attribute of the CSR. CN is the name of the user and O is the group that this user will belong to. 

```
openssl req -new -key sauron.key -out sauron.csr
```
You'll have something like this

![](./csr.png)

We can then proceed to create a certificatresigingrequest and submit it to a Kubernetes Cluster via kubectl. We can do that by usin a here document
```
cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: sauron
spec:
  request: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ2hEQ0NBV3dDQVFBd1B6RUxNQWtHQTFVRUJoTUNSbEl4RHpBTkJnTlZCQWdNQmxOdmNHaHBZVEVPTUF3RwpBMVVFQ2d3RlNVNVNTVUV4RHpBTkJnTlZCQU1NQm5OaGRYSnZiakNDQVNJd0RRWUpLb1pJaHZjTkFRRUJCUUFECmdnRVBBRENDQVFvQ2dnRUJBTDBmWnV4blJkWmxRRU5MMThmWmtIMUFNcUlLS29GK0hpT00zWUFxL0YwdEhiUngKYlYrWnhnU3M1VU5mMDF0TUk3RGtwc2V0SE81T1NadzhoVm5na284blB3UXRVbU94UWJ1U1Z4cUVkVC9zRjY5MgphNytacXFVc2cwZEYyeW0rWkh6MEJ3akRPckJlOUtzSXpETDRSZUlMeklrMWVMOElmaks3aTNZQ3NvZ3RaMURPClhzQ0czT0dYanh6K3VkTVZXdExzcU00SWVKVUg0S094ZTQxTjlmREIybUFLWnA2K0s0Mi9ZWHpDVHFBN3cwVFcKTWVLZDNrUkVsK3ErUHZvSHcwOHVMT3pYcHlnK1RJazM2dnk2WUZRcDYrU2pkd3dVZjFXRGprOERBR0FVaCtvWApjREZwd2N6T2YyWm9vUmRtelNyVnFXdXJ4ZHhDTFpRUTRlU3Jzc3NDQXdFQUFhQUFNQTBHQ1NxR1NJYjNEUUVCCkN3VUFBNElCQVFDdmE3RytVMVg3VnhVUEZzdG9mV2NMMGVvSnNDNFgxbkZyRUp5OEU4QzY4Q0FibkZOK2Q1R04KSzF0TkFhRGcxWDh0aWdqY3BJL1lQNVNoY2tyTFA4Rm5tUW9pQUlsSXdJeXBFK0trRGVHYXZvczRCbFo5MEp6SwpyYy9OU0NxdXpqbmc5dVhzOUlldzEzTkpZRDNyMFlNbHUwOG5nRUFEK3UyM0IyVUxKc0JlUTZTTEY5M0V2aTI1CldtdnBIeFZ6MExUOTMzOHlZcGgwS0JCT1RYUFdKbExDWk1wdXhQYldnZVRSeGJHdkIxS2pxekgxRVVXU243cmwKNXU3cks2RmRWTHYxZW5KSWZnNmJJWnozSDlvei9BQm5BSzVqSzgyTHVYK1VBWHJGd3F1SVBveS9KZDE5OWJGUgpWUURJWUZ1ZUlZQ1F3SHM1RnExZ0FGSTFxdTUxNm45NgotLS0tLUVORCBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0K
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 86400  # one day
  usages:
  - client auth
EOF

```

> + Usage has to be client auth
> + request is the base64 encoded value of the CSR file content. You can get the content using this command: **cat myuser.csr | base64 | tr -d "\n"**
> + Make sure to change name with the right username

We now can approve the CertificateSigningRequest
```
kubectl certificate approve sauron
```
> + You can check that csr was approved with **kubectl get csr**

The certificate value is in Base64-encoded format under status.certificate in the csr.

Export the issued certificate from the CertificateSigningRequest.

```
kubectl get csr sauron -o jsonpath='{.status.certificate}'| base64 -d > sauron.crt
```
### Creating Role and Rolebinding

We now need to define the Role and RoleBinding for this user to access Kubernetes cluster resources.
We create a Role for this new user:

```
kubectl create role getOnly --verb=get --resource=pods --resource=namespaces
```
Then we add a RoleBinding
```
kubectl create rolebinding sauron-getbinding --role=getOnly --user=sauron
```
### Add to kubeconfig

Add the new credentials to set new user
```
kubectl config set-credentials sauron --client-key=sauron.key --client-certificate=sauron.crt --embed-certs=true
```

Now add the context 
```
kubectl config set-context sauron --cluster=kubernetes --user=sauron
```
You can change the context as follows:
```
kubectl config use-context sauron
```

You can see the new user in kubeconfig

```
kubectl config view
```
You'll have something like this: 

![](./configview.png)


### Checking API Access 

kubectl provides the auth can-i subcommand for quickly querying the API authorization layer. The command uses the SelfSubjectAccessReview API to determine if the current user can perform a given action, and works regardless of the authorization mode used.

```
kubectl auth can-i create,get pods --namespace frodo
```
By using sauron context and the command above you should get something like this:
![](./auth-test.png)




