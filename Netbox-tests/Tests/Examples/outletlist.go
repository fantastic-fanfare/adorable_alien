package netbox

import (
	"fmt"
	//"strconv"
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/netbox-community/go-netbox/v3/netbox/client"
	"github.com/netbox-community/go-netbox/v3/netbox/client/dcim"
)

func ListPowerOutlets() {
	transport := httptransport.New("localhost:8008", client.DefaultBasePath, []string{"http"})
	transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 77108240f63427250b77f16c191b32920be02da6")
	c := client.New(transport, nil)
	//devid := "19"
	req := dcim.NewDcimPowerOutletsListParams()
	res, err := c.Dcim.DcimPowerOutletsList(req, nil)
	if err != nil {
		fmt.Printf("Error coudln't list power-outlets: %v\n", err)
	}
	fmt.Printf("List power-outlets:\n")
	for _, v := range res.Payload.Results {
		fmt.Printf("\n%s\n", *v.Name)
	}
}