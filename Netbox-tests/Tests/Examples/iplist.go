package netbox

import (
	"fmt"
	"log"
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/netbox-community/go-netbox/v3/netbox/client"
	"github.com/netbox-community/go-netbox/v3/netbox/client/ipam"
)


func ListIPranges() {
	transport := httptransport.New("localhost:8000", client.DefaultBasePath, []string{"http"})
	transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 7ad29b4266f1ab30099ca79f7a1b531e622e2214")
	c := client.New(transport, nil)
	
	//log.Printf("%+v", c)
	req := ipam.NewIpamIPRangesListParams()
	res, err := c.Ipam.IpamIPRangesList(req, nil)
	if err != nil {
		log.Fatalf("Error getting IP ranges: %v", err)
	}
	//fmt.Printf("Got %v ", res.Payload.Results)
	for _, ipRange := range res.Payload.Results {
		fmt.Printf("Start Address: %s, Size: %d\n", *ipRange.StartAddress, ipRange.Size)
	}
}
