package netbox

import (
	"fmt"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/netbox-community/go-netbox/netbox/client"
	"github.com/netbox-community/go-netbox/netbox/client/dcim"
	"github.com/netbox-community/go-netbox/netbox/models"
)

func SwitchCableStatus() {
	transport := httptransport.New("localhost:8000", client.DefaultBasePath, []string{"http"})
	transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 7ad29b4266f1ab30099ca79f7a1b531e622e2214")
	c := client.New(transport, nil)

	powOutid := int64(24)
	powOuttype := "dcim.poweroutlet"
	terB := &models.GenericObject{
		ObjectID: &powOutid,
		ObjectType: &powOuttype,
	}
	powPorid := int64(23)
	powPortype := "dcim.powerport"
	terA := &models.GenericObject{
		ObjectID: &powPorid,
		ObjectType: &powPortype,
	}
	req := dcim.NewDcimCablesUpdateParams().WithID(88)
	req.SetData(&models.WritableCable{Status: "connected", ATerminations: []*models.GenericObject{terA}, BTerminations: []*models.GenericObject{terB}})
	res, err := c.Dcim.DcimCablesUpdate(req, nil)

	if err != nil{
		fmt.Printf("Couldn't update cable status: %v\n", err)
	}
	fmt.Printf("Updated cable %d to status: %s\n", res.Payload.ID, *res.Payload.Status.Value)
}