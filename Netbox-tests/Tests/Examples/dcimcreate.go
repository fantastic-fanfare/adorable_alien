package netbox

import (
	"fmt"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/netbox-community/go-netbox/netbox/client"
	"github.com/netbox-community/go-netbox/netbox/client/dcim"
	"github.com/netbox-community/go-netbox/netbox/models"
)

func CreateDevice() {
	transport := httptransport.New("localhost:8000", client.DefaultBasePath, []string{"http"})
	transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 7ad29b4266f1ab30099ca79f7a1b531e622e2214")
	c := client.New(transport, nil)

	var (
		devName string = "Tonic"
		powerName string = "v30"
	)

	devRole := &models.NestedDeviceRole{
		ID: 1,
	}
	devType := &models.NestedDeviceType{
		ID: 1,
	}
	devSite := &models.NestedSite{
		ID: 1,
	}

	params := dcim.NewDcimDevicesCreateParams()
	params.SetData(&models.WritableDeviceWithConfigContext{
		Name:       &devName,
		DeviceRole: &devRole.ID,
		DeviceType: &devType.ID,
		Site:       &devSite.ID,
		Status:     "decommissioning",  //status value not label, thus only lower case!!
	})
	res, err := c.Dcim.DcimDevicesCreate(params, nil)
	if err != nil {
		fmt.Printf("Failed to add Device to NetBox: %v", err)
	}

	p := dcim.NewDcimPowerOutletsCreateParams()
	p.SetData(&models.WritablePowerOutlet{
		Device: &res.Payload.ID,
		Name: &powerName,
		MarkConnected: true,
	})
	pres, perr := c.Dcim.DcimPowerOutletsCreate(p, nil)
	if err != nil {
		fmt.Printf("Oups! Failed to add power outlet.. %v", perr)
	}

	//ca := dcim.NewDcimCablesCreateParams()
	//ca.SetData(&models.WritableCable{
	//	Status: "active",
	//	ATerminations: ,
	//})

	fmt.Printf("Added %v to the set of devices with power_outlet %v", *res.Payload.Name, *pres.Payload.Name)
}
