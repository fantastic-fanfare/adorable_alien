package netbox

import (
	"fmt"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/netbox-community/go-netbox/netbox/client"
	"github.com/netbox-community/go-netbox/netbox/client/dcim"
	"github.com/netbox-community/go-netbox/netbox/models"
)

func CreateCable() {
	transport := httptransport.New("localhost:8000", client.DefaultBasePath, []string{"http"})
	transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 7ad29b4266f1ab30099ca79f7a1b531e622e2214")
	c := client.New(transport, nil)

	powOutid := int64(24)
	powOuttype := "dcim.poweroutlet"
	terB := &models.GenericObject{
		ObjectID: &powOutid,
		ObjectType: &powOuttype,
	}
	powPorid := int64(23)
	powPortype := "dcim.powerport"
	terA := &models.GenericObject{
		ObjectID: &powPorid,
		ObjectType: &powPortype,
	}

	reqq := dcim.NewDcimCablesCreateParams()
	reqq.SetData(&models.WritableCable{Status: "decommissioning", Label: "Cable 1", ATerminations: []*models.GenericObject{terA}, BTerminations: []*models.GenericObject{terB}})
	ress, err := c.Dcim.DcimCablesCreate(reqq, nil)

	if err != nil {
		fmt.Printf("Cannot create cable: %v\n", err)
	}
	fmt.Printf("Cable is added with status %s\n", *ress.Payload.Status.Value)
}
