package netbox

import (
	"fmt"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/netbox-community/go-netbox/netbox/client"
	"github.com/netbox-community/go-netbox/netbox/client/dcim"
	//"github.com/netbox-community/go-netbox/netbox/models"
)

func DeleteFirstDevice() {
	transport := httptransport.New("localhost:8000", client.DefaultBasePath, []string{"http"})
	transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 7ad29b4266f1ab30099ca79f7a1b531e622e2214")
	c := client.New(transport, nil)

	req := dcim.NewDcimDevicesListParams()
	res, err := c.Dcim.DcimDevicesList(req, nil)
	if err != nil {
		fmt.Printf("Cannot get Devices list: %v\n", err)
	}
	reponse := res.GetPayload().Results[0]
	delp := dcim.NewDcimDevicesDeleteParams()
	delp.ID = reponse.ID
	_, delerr := c.Dcim.DcimDevicesDelete(delp, nil)
	
	if delerr != nil {
		fmt.Printf("Cannot get delete device! Error: %v\n", err)
	}
	fmt.Printf("The following device is deleted: %v ", *reponse.Name)
}
