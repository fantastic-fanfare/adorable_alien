package netbox

import (
	"fmt"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/netbox-community/go-netbox/netbox/client"
	"github.com/netbox-community/go-netbox/netbox/client/dcim"
	"github.com/netbox-community/go-netbox/netbox/models"
)

func UpdateMarkConnectedPO() {
	transport := httptransport.New("localhost:8000", client.DefaultBasePath, []string{"http"})
	transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 7ad29b4266f1ab30099ca79f7a1b531e622e2214")
	c := client.New(transport, nil)

	param := dcim.NewDcimPowerOutletsListParams()
	res, err := c.Dcim.DcimPowerOutletsList(param, nil)
	if err != nil {
		fmt.Printf("Cannot get list of power-outlets, %v\n", err)
	}
	for _, v := range res.Payload.Results{
		fmt.Printf("Power-outlet: %s, with cable connection: %t\n", *v.Name, *&v.MarkConnected)
	}
	first := res.Payload.Results[0]
	fmt.Printf("\nWe will switch on/off power-outlet with name %s that has cable connection status: %t\n", *first.Name, *&first.MarkConnected)

	req := dcim.NewDcimPowerOutletsPartialUpdateParams()
	if first.MarkConnected {
		req.SetData(&models.WritablePowerOutlet{MarkConnected: false, Name: first.Name, Device: &first.Device.ID})
	} else {
	req.SetData(&models.WritablePowerOutlet{MarkConnected: true, Name: first.Name, Device: &first.Device.ID})
	}
	req.ID = first.ID
	upres, err := c.Dcim.DcimPowerOutletsPartialUpdate(req, nil)
	
	if err != nil {
		fmt.Printf("error updating power outlet : %v\n", err)
	} else if upres.IsSuccess() {
		fmt.Printf("Updated power_outlet (%s) connection to status %t\n", *upres.Payload.Name, *&upres.Payload.MarkConnected)
	}
}