package main

import (
	"fmt"
	//"strconv"
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/netbox-community/go-netbox/v3/netbox/client"
	"github.com/netbox-community/go-netbox/v3/netbox/client/dcim"
	//"github.com/netbox-community/go-netbox/netbox/models"
)


func main() {
	transport := httptransport.New("localhost:8000", client.DefaultBasePath, []string{"http"})
	transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 7ad29b4266f1ab30099ca79f7a1b531e622e2214")
	c := client.New(transport, nil)

	var pduid string = "8"
	req := dcim.NewDcimDevicesListParams().WithRoleID(&pduid)
	res, err := c.Dcim.DcimDevicesList(req, nil)
	if err != nil {
		fmt.Printf("Cannot get PDU devices list: %v\n", err)
	}
	fmt.Printf("We have the the following PDU devices:\n")
	for _, pdu := range res.Payload.Results {
		fmt.Printf("- %v with ID: %d\n", *pdu.Name, pdu.ID)
	}

	poReq := dcim.NewDcimPowerOutletsListParams().WithDeviceID(req.ID)
    poRes, erri := c.Dcim.DcimPowerOutletsList(poReq, nil)
	if erri != nil {
		fmt.Printf("\nCannot get power outlets list for devices: %v\n", erri)
	}

	for _, dev := range res.Payload.Results {
		fmt.Printf("\nPower outlets for device %s: ", *dev.Name)
		for _, po := range poRes.Payload.Results {
			if po.Device.ID == dev.ID {
				fmt.Printf("\tOutlet %s: %t\n", *po.Name, *po.Occupied)
			}
        }
    }
}
