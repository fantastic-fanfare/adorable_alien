package netbox

import (
	"fmt"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/netbox-community/go-netbox/netbox/client"
	"github.com/netbox-community/go-netbox/netbox/client/virtualization"
	"github.com/netbox-community/go-netbox/netbox/models"
)

func ListVM() {
	transport := httptransport.New("localhost:8000", client.DefaultBasePath, []string{"http"})
	transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 7ad29b4266f1ab30099ca79f7a1b531e622e2214")
	c := client.New(transport, nil)

	req := virtualization.NewVirtualizationVirtualMachinesListParams()
	res, err := c.Virtualization.VirtualizationVirtualMachinesList(req, nil)
	if err != nil {
		fmt.Printf("Cannot get VM list: %v\n", err)
	}
	first := res.Payload.Results[0]
	fmt.Printf("VM with name %s has status %s\n", *first.Name, *first.Status.Value)

	updateParams := virtualization.NewVirtualizationVirtualMachinesPartialUpdateParams()
	updateParams.SetData(&models.WritableVirtualMachineWithConfigContext{Status: "planned", Name: first.Name})
	updateParams.ID = first.ID

	updateRes, err := c.Virtualization.VirtualizationVirtualMachinesPartialUpdate(updateParams, nil)

	if updateRes.IsSuccess() {
		fmt.Printf("success\n")
		fmt.Printf("Updated VM with name %s to status %s\n", *updateRes.Payload.Name, *updateRes.Payload.Status.Value)
	}
}