package main

import (
	//"fmt"
	//"strings"
	//"time"
	//"os"
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	//"github.com/dgrijalva/jwt-go"
	//"github.com/mitchellh/mapstructure"
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/netbox-community/go-netbox/v3/netbox/client"
	"github.com/netbox-community/go-netbox/v3/netbox/client/ipam"
)

func ListIpRanges(c *client.NetBoxAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := ipam.NewIpamIPRangesListParams()
		res, err := c.Ipam.IpamIPRangesList(req, nil)
		if err != nil {
			log.Fatalf("Error getting IP ranges: %v", err)
		}
		//fmt.Printf("Got %v ", res.Payload.Results)
		for _, ipRange := range res.Payload.Results {
			json.NewEncoder(w).Encode(map[string]interface{}{
				"Start Adress": *ipRange.StartAddress,
				"Size":         ipRange.Size,
			})
		}
	}
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Super Secret information for valid users only to access !!!")
}



func main() {
	host := flag.String("host", "localhost", "Required flag, must be the hostname that is resolvable via DNS, or 'localhost'")
	port := flag.String("port", "443", "The https port, defaults to 443")
	certFile := flag.String("certfile", "./localhost.pem", "certificate file")
	keyFile := flag.String("keyfile", "./localhost-key.pem", "key file")
	flag.Parse()

	transport := httptransport.New("localhost:8000", client.DefaultBasePath, []string{"http"})
	transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 7ad29b4266f1ab30099ca79f7a1b531e622e2214")
	c := client.New(transport, nil)

	if *host == "" || *certFile == "" || *keyFile == "" {
		log.Fatalf("One or more required fields missing: serverCertFile, serverKeyFile, hostname or port")
	}

	r := mux.NewRouter()
	r.HandleFunc("/iprange", ListIpRanges(c)).Methods("GET")
	//r.HandleFunc("/", isAuthorized(homePage))
	srv := &http.Server{
		Addr:    ":" + *port,
		Handler: r,
		TLSConfig: &tls.Config{
			MinVersion:               tls.VersionTLS13,
			PreferServerCipherSuites: true,
		},
	}

	log.Printf("Starting HTTPS server on host %s and port %s", *host, *port)
	if err := srv.ListenAndServeTLS(*certFile, *keyFile); err != nil {
		log.Fatal(err)
	}
}
