package util

import(
	"time"
	"github.com/spf13/viper"
)

//Config stores all configuration of the application.
//The values are read by viper from a config file or env variable.
type Config struct {
	TokenSymmetricKey string `mapstructure:"TOKEN_SYMMETRIC_KEY"`
	AccessTokenDuration time.Duration `mapstructure:"ACCESS_TOKEN_DURATION"`
}

//LoadConfig reads configuration from file or env variable using viper
func LoadConfig(path string) (config Config, err error){
	viper.AddConfigPath(path)
	viper.
}