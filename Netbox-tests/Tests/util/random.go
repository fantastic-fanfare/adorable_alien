package util

import(
	"time"
	"math/rand"
	"strings"
)

const alphabet = "abcdefghijklmnopqrstuvwxy"

func inti(){
	rand.Seed(time.Now().UnixNano())
}

//RandomInt generates a random integer between min and max	
func RandomInt(min, max int64) int64{
	return min + rand.Int63n(max - min + 1) //min + rand[0, max-min] = rand[min,max]
}

//RandomString generates a random string of length n
func RandomString(n int) string {
	var sb strings.Builder
	k:= len(alphabet)

	for i := 0; i < n; i++ {
		c:= alphabet[rand.Intn(k)] //random alphabet position between 0 and k-1
		sb.WriteByte(c)
	}
	return sb.String()
}

func RandomClient() string{
	return RandomString(6)
}

