package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/joho/godotenv"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/gorilla/mux"
	"github.com/netbox-community/go-netbox/v3/netbox/client"
	"github.com/netbox-community/go-netbox/v3/netbox/client/dcim"
	"github.com/netbox-community/go-netbox/v3/netbox/models"
)

func getDevices(c *client.NetBoxAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := dcim.NewDcimDevicesListParams()
		res, err := c.Dcim.DcimDevicesList(req, nil)
		if err != nil {
			log.Fatal(err)
			fmt.Printf("Cannot get Devices list: %v\n", err)
		}
		for _, dev := range res.Payload.Results {
			json.NewEncoder(w).Encode(map[string]interface{}{
				"ID":     dev.ID,
				"name":   dev.Name,
				"type":   dev.DeviceType.Display,
				"status": dev.Status.Value,
				"site":   dev.Site.Name,
				// You can add as many properties to display as you want here
			})
		}
	}
}

func getDevice(c *client.NetBoxAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		deviceID := string(params["id"])
		req := dcim.NewDcimDevicesListParams().WithID(&deviceID)
		res, err := c.Dcim.DcimDevicesList(req, nil)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			fmt.Printf("Cannot get the desired device: %v\n", err)
		}
		for _, dev := range res.Payload.Results {
			json.NewEncoder(w).Encode(map[string]interface{}{
				"id":     dev.ID,
				"name":   dev.Name,
				"type":   dev.DeviceType.Display,
				"status": dev.Status.Value,
				"site":   dev.Site.Name,
			})
		}
	}
}

func getDevicePowerOutlet(c *client.NetBoxAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var pduid string = "8"
		req := dcim.NewDcimDevicesListParams().WithID(&pduid)
		res, err := c.Dcim.DcimDevicesList(req, nil)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			fmt.Printf("Cannot get the desired device: %v\n", err)
		}
		for _, dev := range res.Payload.Results {
			// Get power outlets for current device
			d := strconv.FormatInt(dev.ID, 10)
			poReq := dcim.NewDcimPowerOutletsListParams().WithDeviceID(&d)
			poRes, erri := c.Dcim.DcimPowerOutletsList(poReq, nil)
			if erri != nil {
				log.Fatal(erri)
				fmt.Printf("Cannot get power outlets list for device %s: %v\n", *dev.Name, erri)
				continue
			}

			// Print power outlets and their status
			fmt.Printf("Power outlets for device %s:\n", *dev.Name)
			for _, po := range poRes.Payload.Results {
				json.NewEncoder(w).Encode(map[string]interface{}{
					"device":      dev.Name,
					"outlet name": po.Name,
					"occupied":    po.Occupied,
				})
			}
		}
	}
}

func deleteDevice(c *client.NetBoxAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		deviceID, _ := strconv.Atoi(params["id"])
		req := dcim.NewDcimDevicesDeleteParams().WithID(int64(deviceID))
		_, err := c.Dcim.DcimDevicesDelete(req, nil)
		if err != nil {
			log.Fatal(err)
			fmt.Printf("Cannot delete device: %v\n", err)
		}
		json.NewEncoder(w).Encode("Deleted device successfully :)")
	}
}

func createCable(c *client.NetBoxAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//Parse the request body into a WritableCable struct
		var body models.WritableCable
		json.NewDecoder(r.Body).Decode(&body)
		//The body shoudl be correctly wrriten such that it respect the mapping of fields. It must include a/b terminations [{object_id and object_type}]
		req := dcim.NewDcimCablesCreateParams().WithData(&body)
		_, err := c.Dcim.DcimCablesCreate(req, nil)
		if err != nil {
			log.Fatal(err)
			fmt.Printf("Cannot create cable: %v\n", err)
		}
		json.NewEncoder(w).Encode("Cable successfully added :D You can visualize it in your NetBox dashboard")
	}
}

func updateCable(c *client.NetBoxAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		cabID, _ := strconv.Atoi(params["id"])
		var body models.WritableCable
		json.NewDecoder(r.Body).Decode(&body)
		// Get the current cable object to preserve A and B terminations
		getReq := dcim.NewDcimCablesReadParams().WithID(int64(cabID))
		getRes, err := c.Dcim.DcimCablesRead(getReq, nil)
		if err != nil {
			fmt.Printf("Cannot get cable / Does not exist probably!: %v\n", err)
			return
		}
		curCable := getRes.Payload
		req := dcim.NewDcimCablesUpdateParams().WithID(int64(cabID))
		req.SetData(&models.WritableCable{Status: body.Status, ATerminations: curCable.ATerminations, BTerminations: curCable.BTerminations})
		_, errr := c.Dcim.DcimCablesUpdate(req, nil)
		if errr != nil {
			log.Fatal(errr)
			fmt.Printf("Cannot update cable: %v\n", errr)
		}
		json.NewEncoder(w).Encode("Cable successfully updated :D You can visualize changes in your NetBox dashboard")

	}
}

func main() {
	//Configuring my server certif and key to use for TLS connections
	host := flag.String("host", "localhost", "Required flag, must be the hostname that is resolvable via DNS, or 'localhost'")
	port := flag.String("port", "443", "The https port, defaults to 443")
	certFile := flag.String("certfile", "./localhost.pem", "certificate file")
	keyFile := flag.String("keyfile", "./localhost-key.pem", "key file")
	flag.Parse()

	if *host == "" || *certFile == "" || *keyFile == "" {
		log.Fatalf("One or more required fields missing: serverCertFile, serverKeyFile, hostname or port")
	}

	//Creating netbox client to communicate with NetBox API server using api-token and host name stored in .env. We thus created a transport connection for our client.
	godotenv.Load()
	token := os.Getenv("NETBOX_TOKEN")
	if token == "" {
		log.Fatalf("Please provide netbox API token via env var NETBOX_TOKEN")
	}
	netboxHost := os.Getenv("NETBOX_HOST")
	if netboxHost == "" {
		log.Fatalf("Please provide netbox host via env var NETBOX_HOST")
	}
	t := httptransport.New(netboxHost, client.DefaultBasePath, []string{"https"})
	t.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token "+token)
	c := client.New(t, nil)

	//Initiating mux router to handle our request
	r := mux.NewRouter()
	r.HandleFunc("/netbox/dcim/devices", getDevices(c)).Methods("GET")
	r.HandleFunc("/netbox/dcim/devices/pdu/power-outlet", getDevicePowerOutlet(c)).Methods("GET")
	r.HandleFunc("/netbox/dcim/devices/{id}", getDevice(c)).Methods("GET")
	r.HandleFunc("/netbox/dcim/cables", createCable(c)).Methods("POST")
	r.HandleFunc("/netbox/dcim/cables/{id}", updateCable(c)).Methods("PUT")
	r.HandleFunc("/netbox/dcim/devices/{id}", deleteDevice(c)).Methods("DELETE")

	//log.Fatal(http.ListenAndServe(":8080", nil)) for a simple http server
	//Here we will set up our server config and start it using TLS connection components we defined above
	srv := &http.Server{
		Addr:    ":" + *port,
		Handler: r,
		TLSConfig: &tls.Config{
			MinVersion:               tls.VersionTLS13,
			PreferServerCipherSuites: true,
		},
	}

	log.Printf("Starting HTTPS server on host %s and port %s", *host, *port)
	if err := srv.ListenAndServeTLS(*certFile, *keyFile); err != nil {
		log.Fatal(err)
	}
}
