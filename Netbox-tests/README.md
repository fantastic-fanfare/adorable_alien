# Hands-On 2: Go REST API with NetBox-Docker API

In this second tutorial, we will follow the same objectif of quering and retrieving information as we did in a simple first [hands-on](https://gitlab.inria.fr/fantastic-fanfare/adorable_alien/-/tree/develop/Hands-on%201%3A%20Go%20REST%20API%20with%20PostgreSQL%20and%20Mux), but now from our NetBox-Docker instance by calling a netbox client.

Golang proposes a handful library called `go-netbox` that we will use in this lab and where all the functions, types and constants are defined.

Before starting, you can check the official documentation for the NetBox-Docker [here](). As well as the [guide]() to install all the containers needed to initiate your Docker NetBox. After running your instance, you can craete a super user (admin) and start manipulating and creating devices (dcim) or any other component. For a concrete touch, we have restored a SQL dump file in Posgres container as a snapshot of Sop-node infrastructure for better illustration of our user story.

## Prerequisites

- Golang and Docker familiarity
- Some PostgreSQL basics
- REST API familiarity

## Restoring Data inside container

#### Option 1:
This is the part where we restored a dump file `netbox_20230406_090001.sq1` (provided below) that should initially reside in your working directory where all the containers remain. We used the following command:
```
docker exec -it <postgres-container-name> psql -U netbox -d netbox -f docker-entrypoint-initdb.d/netbox_20230406_090001.sq1
```

**PS**: Note that the file is large enough to require more memory to be fully restored.

#### Option 2:
If you want to explore NetBox yourself, you can follow their official documentation and free introductory lab to be able to create data yourself using NetBox dashboard which is super simplified and easy to use. This way you will also understand its background concept by examining NetBox API (bottom left of the dashboard) and the different existing relationships between NetBox components.

## Calling Netbox Client 
To be able to interact with our NetBox instance, we will make calls to its client API that will afterwards handle our requests. Hence, your NetBox API **token** is required (something like **7ad29b4266f1ab30099ca79f7as3rg8767214**) along with the hostname and port (something like **localhost:8000**). Here is an example (in Go) of how to define a transport for our client API supporting HTTP. It can also support HTTPS, you just need to replace "http" by "https".

Of course, the appropriate packages should be installed beforehand using `go get` knowing that you initiated a **go Module** first (handles dependencies of our code):
```
go get github.com/go-openapi/runtime/client
go get github.com/netbox-community/go-netbox/netbox/client
```
Then, we are good to go:

```go
import (
    httptransport "github.com/go-openapi/runtime/client"
    "github.com/netbox-community/go-netbox/netbox/client"
)

transport := httptransport.New("localhost:8000", client.DefaultBasePath, []string{"http"})
transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 7ad29b4266f1ab30099ca79f7as3rg8767214")
c := client.New(transport, nil)
```

## Requests Example
As a start, we will have a glimpse on a set of examples we have provided for you in order to get hands dirty at first before digging deeper into the main API. Check `/Examples/netbox_req` for more information.

Let's tackle an example and explain the logic behind the code. We also recommend to check the documentation of `go-netbox` framework for ease of understanding.

```go
package netbox

import (
	"fmt"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/netbox-community/go-netbox/netbox/client"
	"github.com/netbox-community/go-netbox/netbox/client/virtualization"
	"github.com/netbox-community/go-netbox/netbox/models"
)

func UpdateVM() {
	transport := httptransport.New("localhost:8000", client.DefaultBasePath, []string{"http"})
	transport.DefaultAuthentication = httptransport.APIKeyAuth("Authorization", "header", "Token 7abdv698dbjs96f1ab30099ca7531e622e2214")
	c := client.New(transport, nil)

	req := virtualization.NewVirtualizationVirtualMachinesListParams()
	res, err := c.Virtualization.VirtualizationVirtualMachinesList(req, nil)
	if err != nil {
		fmt.Printf("Cannot get VM list: %v\n", err)
	}
	first := res.Payload.Results[0]
	fmt.Printf("VM with name %s has status %s\n", *first.Name, *first.Status.Value)

	updateParams := virtualization.NewVirtualizationVirtualMachinesPartialUpdateParams()
	updateParams.SetData(&models.WritableVirtualMachineWithConfigContext{Status: "planned", Name: first.Name})
	updateParams.ID = first.ID

	updateRes, err := c.Virtualization.VirtualizationVirtualMachinesPartialUpdate(updateParams, nil)

	if updateRes.IsSuccess() {
		fmt.Printf("success\n")
		fmt.Printf("Updated VM with name %s to status %s\n", *updateRes.Payload.Name, *updateRes.Payload.Status.Value)
	}
}
```
The code above will first of all initiate our client API. Secondly, il will launch the request to retrieve the list of VMs from Virtualization section. However, we are only interested in the first VM of the list. So we will print the response grepping on the VM's name and status. That was to make sure that the first VM we want to update actually exists.
Next, we will launch a second request (updateParams) to update one or more parameters of this VM by refering to its ID. In this example we are updating its **status** value to _planned_. The responses of our requests are handled by our client API for us. Lastly, we will make sure that the update which is the response back form the client was successful and thus print it.

The other examplles showcase the other CRUD operations of differnet components such as devices, IP ranges, power-outlets and power-ports...

## Implementation in API
If you understood the examples, it would be easy to build the API because the main principle of requesting and getting answers from the client API are unchangeable. Hence, we highly recommend to grasp the latter before moving forward to this section.

Take a look at `/Api_NetBox_tls/main.go` file or import it. You can play around with your own personalized requests if the examples inspired you.

As we are in a cloud native context, we can package our application in a docker image to make is easy to pull and build for users.

### TLS configuration
We configured HTTPS connection in our backend by using self-signed certificate (CA) that I generated locally with a go framework called `mkcert`. The purpose of this configuration was just for the simple matter of testing. The objective is to have a public CA trusted by Inria that will generate certificates. We discussed this idea in another section related to [PKI](https://gitlab.inria.fr/fantastic-fanfare/adorable_alien/-/tree/develop/PKI) where we taught about how to securily design our PKI infra: how to manage CAs generation in the whole SCLICES servers environment.