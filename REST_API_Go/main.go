package main

import (
	"database/sql"  // To connect to sql db
	"encoding/json" // To encode data to json
	"fmt"           // To print out information about servers and connections
	"log"           // To log out errors
	"net/http"      // To handle http requests
	"os"            // To get variables

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

type User struct {
	ID           string `json:"id"` // json mapping to be able to encode and decode json with postamn
	ICCID        string `json:"name"`
	IMSI string    `json:"outlet_number"`
	LAI       string `json:"status"`
	K string `json:"k"`
	OEN string `json:"oen"`
}

var db *sql.DB

// The following functions are used to handle HTTP requests

func getSimList(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		rows, err := db.Query("SELECT * FROM users")
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()

		for rows.Next() {
			var u User
			if err := rows.Scan(&u.ID, &u.ICCID, &u.IMSI, &u.LAI, &u.K, &u.OEN); err != nil {
				log.Fatal(err)
			}
			if err := rows.Err(); err != nil {
				log.Fatal(err)
			}
			json.NewEncoder(w).Encode(u)
		}
	}
}

func createSimUser(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var u User
		json.NewDecoder(r.Body).Decode(&u)                                                                                               //Decode the JSON body of the new machine that is sent to Postman. The decoded value will be stored in  the var <m>
		_, err := db.Exec("INSERT INTO users (id, ICCID, IMSI, LAI, K, OEN) VALUES (?, ?, ?, ?, ?, ?)", u.ID, u.ICCID, u.IMSI, u.LAI, u.K, u.OEN) //add the new machine to our table with its given characteristics
		if err != nil {
			log.Fatal(err)
		}
		json.NewEncoder(w).Encode(u)
	}
}

// Indide the main function we will have to define the router and functions (to do CRUD operations)

func main() {

	//Connection to postgres database
	db, err := sql.Open("simcards", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatalf("Could not connect to database: %v", err)
	}
	defer db.Close()

	r := mux.NewRouter()
	r.HandleFunc("/simlist", getSimList(db)).Methods("GET")
	r.HandleFunc("/sim-login", createSimUser(db)).Methods("POST")


	fmt.Printf("Starting the server at port 8008\n")
	log.Fatal(http.ListenAndServe(":8008", r))

}
