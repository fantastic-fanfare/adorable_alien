## Introduction

Designing a secure system is always a headache especially if you are dealing with Public Key Infrastructure. In this article, we will be tackeling and explaining how to manage the generation of CAs in a private environment where multiple servers exchange VPN connections and should be able to authenticate and authorize fellow servers/clients to connect to them.

## What is CA and how it works?

Before defining what a CA is, let us first pull the global context of the latter: Public Key Infrastructure. In few lines, PKI is a system of digital certificates, public key encryption, and other cryptographic protocols that enables secure communication over the internet. At the heart of PKI is the Certificate Authority (CA), which is a trusted entity that issues digital certificates that are used to verify the identity of users, devices, and servers. These digital certificates contain a public key that can be used to encrypt and authenticate data, and they are digitally signed by the CA to establish trust in their authenticity.  

Resources: http://thesslstore.com/blog/pki-architecture-fundamentals-of-designing-a-private-pki-system/  

Upon a request from client/browser to access a server, the latter will share in response its signed certificate (that a CA signed its CSR) and public key. The client can then verify the signature with the public key of the authaurity that signed the certificate (with its private key), so that after, a secret key will be generated (by the client) and encrypted with the public key of the end server. On the other end, only the server can decrypt this secret key with its private key.
At this point, we will have to distinguish between 2 types of architectures: public (public CA) and private public CA) PKIs. Refer to the next section for better understanding.

## Public vs Private PKI/CA

Don’t worry, both configurations offer the same security features, they simply differ in the scope of implementation and auditing. In a nutshell, here are the main differences:
- A private PKI allows you to create your own internal CA and issue digital certificates that are trusted only within your organization. This means that you have complete control over the issuance and revocation of certificates, and you can ensure that they are only used by authorized users and devices. The obvious downside to a private CA is that you have to set up and run the infrastructure yourself.
- Using a public PKI, on the other hand, a global infrastructure that is managed by third-party commercial entities, such as Symantec, Comodo, and GlobalSign. These entities act as trusted intermediaries between organizations and individuals, issuing digital certificates that are recognized and trusted by a wide range of software applications and devices.

If you foresee needing to issue a high volume of certifications, either because the organization is massive or the certs will need to be reissued frequently, it can be cheaper to run your own internal CA than to pay for every one issued by a public CA despite the hassle of local PKI maintenance.
In our sepcial case and according to the fondamental hierachy of CAs, we taught of using a public root CA and its subordinate or intermediate CA which has the same measure of trust afforded to the root because it is validated by it. Not only that, but we also have to think about the availability of a persistant and highly secure storage in where private keys and revocation list (CRL) for example should safely remain.

There is also the possibility to create a self-signed certificate (your own CA) when you want to deploy your apps to staging environment for instance. But the latter will not be our focus for this talk, so please refer to further resources for more details.
In this article, we will discuss some implementation scenarios that are present on the market, each will be demystified and reported on budget, implementation and compliance wise according to our system needs.

## 3 Case Scenarios: AWS, Thales, TCS Sectigo

### Scenario1: Third Party AWS Cloud HSM
AWS offers interesting cloud-based solutions that are scalable, reliable, and highly available cloud infrastructure all tailored to meet the needs of any business. One of the tenet services is AWS CloudHSM, a cryptographic service providing hardware security modules (HSMs) that help generate and use your own encryption keys in AWS within your on-premises servers or applications hosted on AWS. This way, you enforce the security of your CAs. 

how it works? First, you provision CloudHSM cluster in the CloudHSM Console. You automatically create an HSM cluster which contains multiple synchronized and load balanced HSMs. So you receive dedicated, single-tenant access to each HSM in your cluster. Each HSM appears as a network resource in your Amazon Virtual Private Cloud (VPC). Adding and removing HSMs from your Cluster is a single call to the AWS CloudHSM API (or on the command line using the AWS CLI). After creating and initializing a CloudHSM Cluster, you can configure a client on your EC2 instance that allows your applications to use the cluster over a secure, authenticated network connection. Cryptographic APIs are the bridge fo exchange between the client and the apps.
The HSM client software, installed in our applications, maintains a secure channel to all of the HSMs in your cluster and sends requests on this channel, and the HSM performs the operations and returns the results over the secure channel.

The only matter is that this service actually must be provisionned inside a VPC instance along with a secure connection (VPN) between teh latter and our on premise severs. The CloudHSM software client must installed on our servers to be able to use the standard APIs supported by CloudHSM, including PKCS#11 and Java JCA/JCE (Java Cryptography Architecture/Java Cryptography Extensions), or Microsoft CAPI/CNG. 
Payment by the hour for each HSM you launch until you terminate the HSM. For EU-Paris region, the cost is 2.18$/h. So for 2 HSMs for example we get the following monthly cost of: HSM (monthly): 3,182.80 USD.

**Resource: https://aws.amazon.com/cloudhsm/?nc2=h_ql_prod_se_chsm**

### Scenario2: Thales HSM Luna + INRIA persitent storage
We know that INRIA has a persistent storage but not secure. Using an additional Thales HSM hardware on top will provide a higher level of security when comes to storing keys and CAs.
A Thales HSM is a dedicated hardware device that is designed to securely store cryptographic keys and perform cryptographic operations where the keys never leave the intrusion-resistant, tamper-evident, FIPS-validated appliance. To use it, we can use Thales SDKs and APIS to integrate the HSM in our systems and software. In addition to providing secure storage for CAs and private keys, a Thales HSM can also securely store and sign revocation lists to provide additional assurance that the lists are authentic and have not been altered.

Thales proposes Luna HSM is a leader in the market performance wise especially when comes to protecting SSL/TLS keys. Also, it enables over 20,000 ECC and 10,000 RSA Operations per second with very low latency. 
**Additional Features available here: https://cpl.thalesgroup.com/encryption/hardware-security-modules/network-hsms**

Thales Luna HSM is very reliable with a Mean Time Between Failure (MTBF) equals to 171,308 hrs (19.5 years).

So, in regards to our case, Thales Cloud HSM will take care of storing and managing the private keys of our root CA. Since the latter is a public one, it can be hosted anywhere. On the other hand, the sub CA that normally resides in a pod inside the k8s cluster in sophia node for example, should also access Thales HSM for storage and management purposes. To achieve that, a software called Thales Luna Client must be installed on the server hosting the pod (sop-node) to provide the pod with an interactive interface with the HSM. Hence configuring parameters in the software to point to the Luna HSM, then the CA to use the latter as its key store by specifying the PKCS#11 (a standard cryptographic interface that allows applications to use cryptographic functions provided by a hardware security module) for Luna HSM. Lastly, we should mount the PKCS#11 library provided by the software into the pod of the CA so that it uses the mounted library as key storage provider.
The CA is finally responsible for certificates revocation (CRLs) which is not guaranteed with Thales HSM.

The cost of Thales Luna HSM varies depending on several factors, such as the number of HSMs required, the specific model of HSM, the level of performance and capacity needed, and the specific features and services required. Thales does not provide specific pricing information on their website, as the cost is typically based on individual customer requirements and is often determined through consultation with a Thales sales representative. However, the pricing can vary between £23,000.00 – £38,100.00 (https://crypto-store.net/shop/thales-luna-s700-series/)

### Scenario3: Third Party GÉANT (TCS/Sectigo)
The GEANT Association is the pan-European data network provider for the research and education community. And TERENA Certificate Service (TCS) is one of the services provided by GEANT, which offers digital certificates for the latter community in Europe. TCS provides a cost-effective and secure way to obtain not only SSL/TLS, but also S/MIME, and code signing certificates for organizations within the research and education sphere.

From 2020, TCS moved to partner with Sectigo, one of the largest global Certificate Authorities for public or private web servers and load balancers, to have full technologies and services needed to support the infrastructure. One of the main advantages of this coupling is that the participating organisations get to issue close to unlimited numbers of certificates provided by a commercial CA at a very reduced price. The other good news is that Sectigo handles the certificate installation and creation thanks to its SCM manager instead of the traditional DigiCert for example. Plus the high level of security provided to processes of generating and managing certificates and that includes the presence of HSMs and rigorous authentication procedures. We recall that HSMs are specialized physical devices designed specifically for secure key storage and management.

Sectigo also offers an automated certificate management environment ACME, which is a client collocated with web server or LBsm and is actually between CAs and applications using Public SSL.

**Resource: https://wiki.geant.org/display/TCSNT/TCS+Training?preview=/154993489/256475188/ACME%20GEANT%20Presentation%20Apr%2013%202021%20.pdf**

Hence, we have the eligibility to use the TCS service to manage the generation and renewal of trusted digital SSL/TLS certificates for our netowrk of servers. The pain of manual monitoring is also solved by the user-friendly software/dashboard that simplifies managing your certificates in terms of issuance, revocation, reporting and auditing capabilities. Here we are talking about ACME, the core component of Sectigo that stands for Automated Certificate Managment Environment.
TCS by Sectigo also provides the ability to revoke certificates and manage certificate revocation lists (CRLs) to ensure the integrity of the certificate chain. However, the ability to deny a client from revoking a certificate depends on the specific policies and access controls that are in place within the organization's PKI infrastructure. TCS provides the tools and mechanisms to enforce these policies and access controls, but it is ultimately up to the organization to implement them appropriately.
When comes to budget, the cost of using the GEANT Trusted Certificate Service (TCS) provided by Sectigo will depend on several factors, such as the number of certificates issued, the level of assurance required. However, as a member of an NREN, such as RENATER, INRIA and connected institues could be eligible for discounted or free access to TCS.



