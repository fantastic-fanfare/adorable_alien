# The Clear BSD License
# 
# Copyright (c) 2023 Damien Saucez
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the disclaimer
# below) provided that the following conditions are met:
# 
#      * Redistributions of source code must retain the above copyright notice,
#      this list of conditions and the following disclaimer.
# 
#      * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
# 
#      * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from this
#      software without specific prior written permission.
# 
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
# THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
# CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import igraph

class Topo:
  """
  Network topology to be used to generate configurations
  """

  def __init__(self):
    """
    Create an empty network toplogy
    """
    # The topology is represented with a graph
    self.g = igraph.Graph(directed=False)

  def addNode(self, name, **attrs):
    """
    Add a node to the topology

    :arg str name: name of the node
    :arg attributes of the node:

    :returns igraph.Vertex: the created node
    """
    return self.g.add_vertex(name, **attrs)

  def addLink(self, a, b, **attrs):
    """
    Connect the two nodes a and b with a symetrical link

    An edge between a and b is added to self.g with extra labels defined by
    `attrs`

    :arg igraph.Vertex a: first node
    :arg igraph.Vertex b: second node
    :arg attrs: attributes of the link

    :returns igraph.Edge: the created link
    """
    edge = self.g.add_edge(a.index, b.index)

    attrs is not None and edge.update_attributes(attrs)

    return edge

  def drawTopology(self, filename='topo.svg', root=[]):
    """
    Draw the topology in a file

    :arg str filename: filename to dump to
    :arg list root: index of the nodes to be used as a root of the drawing.
                    Ids are constructed in sequence from 0 at their addition
                    with `addNode`. Default is `[]`
    :arg str plot: plot filename, file extension must be compatible with Cairo
                   library. Default is `topo.svg`
    """
    layout = self.g.layout_reingold_tilford(root=root)
    igraph.plot(self.g, filename, layout=layout, vertex_label=self.g.vs['name'], bbox=(1024, 768), margin=100)

  def dumpTopology(self, filename='topo.gml'):
    """
    Dump the topology to a GML graph file.

    :arg str filename: filename to dump to. Default is `topo.gml`
    """
    self.g.write_gml(filename)

  def getNodes(self):
    """
    Get all the node of the topology

    :returns igraph.seq.VertexSeq: list of nodes
    """
    return self.g.vs

  def getLinks(self):
    """
    Get all the links of the topology

    :returns igraph.seq.EdgeSeq: list of links
    """
    return self.g.es