Creator "igraph version 0.10.4 Thu Mar 30 21:00:17 2023"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    type "spine"
    name "spine_0"
  ]
  node
  [
    id 1
    type "spine"
    name "spine_1"
  ]
  node
  [
    id 2
    type "leaf"
    name "leaf_0"
  ]
  node
  [
    id 3
    type "leaf"
    name "leaf_1"
  ]
  node
  [
    id 4
    type "compute"
    name "node_0"
  ]
  node
  [
    id 5
    type "compute"
    name "node_1"
  ]
  node
  [
    id 6
    type "compute"
    name "node_2"
  ]
  node
  [
    id 7
    type "compute"
    name "node_3"
  ]
  node
  [
    id 8
    type "compute"
    name "node_4"
  ]
  node
  [
    id 9
    type "compute"
    name "node_5"
  ]
  node
  [
    id 10
    type "compute"
    name "node_6"
  ]
  node
  [
    id 11
    type "compute"
    name "node_7"
  ]
  edge
  [
    source 2
    target 0
    capacity 100
  ]
  edge
  [
    source 3
    target 0
    capacity 100
  ]
  edge
  [
    source 2
    target 1
    capacity 100
  ]
  edge
  [
    source 3
    target 1
    capacity 100
  ]
  edge
  [
    source 4
    target 2
    capacity 25
  ]
  edge
  [
    source 4
    target 3
    capacity 25
  ]
  edge
  [
    source 5
    target 2
    capacity 25
  ]
  edge
  [
    source 5
    target 3
    capacity 25
  ]
  edge
  [
    source 6
    target 2
    capacity 25
  ]
  edge
  [
    source 6
    target 3
    capacity 25
  ]
  edge
  [
    source 7
    target 2
    capacity 25
  ]
  edge
  [
    source 7
    target 3
    capacity 25
  ]
  edge
  [
    source 8
    target 2
    capacity 25
  ]
  edge
  [
    source 8
    target 3
    capacity 25
  ]
  edge
  [
    source 9
    target 2
    capacity 25
  ]
  edge
  [
    source 9
    target 3
    capacity 25
  ]
  edge
  [
    source 10
    target 2
    capacity 25
  ]
  edge
  [
    source 10
    target 3
    capacity 25
  ]
  edge
  [
    source 11
    target 2
    capacity 25
  ]
  edge
  [
    source 11
    target 3
    capacity 25
  ]
]
