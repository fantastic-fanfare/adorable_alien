# The Clear BSD License
# 
# Copyright (c) 2023 Damien Saucez
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the disclaimer
# below) provided that the following conditions are met:
# 
#      * Redistributions of source code must retain the above copyright notice,
#      this list of conditions and the following disclaimer.
# 
#      * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
# 
#      * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from this
#      software without specific prior written permission.
# 
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
# THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
# CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
 
from topo import Topo

def spineLeaf(s, l, n):
  """
  Create a spine and leaf topology with 100Gbps links to spine nodes and 25Gbps
  links to compute nodes.

  :arg int s: number of spine routers
  :arg int l: number of leaf routers
  :arg int n: number of comnpute nodes

  :returns Topo: the topology
  """
  spines = []
  leafs = []
  nodes = []

  topo = Topo()

  # Spine routers
  for i in range(s):
    spine = topo.addNode('spine_{}'.format(i), type='spine')
    spines.append(spine)

  # Leaf routers
  for i in range(l):
    leaf = topo.addNode('leaf_{}'.format(i), type='leaf')
    leafs.append(leaf)

  # Compute nodes
  for i in range(n):
    node = topo.addNode('node_{}'.format(i), type='compute')
    nodes.append(node)

  # addLink the spines
  for spine in spines:
    for leaf in leafs:
      topo.addLink(spine, leaf, capacity=100)

  # addLink the nodes 
  for node in nodes:
    for leaf in leafs:
      topo.addLink(node, leaf, capacity=25)

  return topo

def main():
  """
  Create a spine-leaf topology, 
  """
  s=2
  l=2
  n=8
  topo = spineLeaf(s=s, l=l, n=n)

  # Print the name of all the nodes in the topology and their type
  for node in topo.getNodes():
    print('{} is a {}'.format(node['name'], node['type']))

  topo.dumpTopology(filename='topo.gml')
  topo.drawTopology(root=range(0,s), filename='topo.svg')

if __name__ == "__main__":
  main()
