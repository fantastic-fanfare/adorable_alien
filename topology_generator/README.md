# Topolgy generation

The `Topo` class is used to define, save, and draw topologies in a programatic
way.

A topology is represented as a graph where switches, routers, or servers are
represented by verticies and links are represented by edges. 

A node has a name and an arbitrary number of attributes. Nodes are added with
the `addNode` method.

Only point to point links are supported. Links are between two nodes and have
an arbitrary number of attributes. Links are added with the `addLink` method.

Toplogies can be drawn as images using the `drawTopology` method. File
extensions can be any extension supported by the Cairo library (e.g., SVG, PNG,
EPS, pdf).

To be exported to other tools, the topologies can be exported in GML format with
the `dumpTopology` method.

The `example.py` file provides an example of topology defintion and dump. It is
used to build a parametric spine-leaf topology. The figure below shows a
representation of the topology.

![Spine-Leaf topology](topo.svg)

## Dependencies
To install dependencies, run the following command:

```
pip3 install -r requirements.txt 
```